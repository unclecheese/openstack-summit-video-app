## Micro project for the Summit Video App

### Installation

* checkout the repository
* `composer install`
* configure your environment as necessary in an `_ss_environment.php` file
* go to the `summit-video-app/` directory
* `npm install`
* Ask Aaron for a database dump
* Load that into your database
* Win


### Workflow

To develop, run `npm run start` in the `summit-video-app/` directory. This will initiate a Webpack dev server that will hotload any changes to CSS or React components.

To deploy, run `npm run build` to create a production build.

# Warning






## Seriously







### No joke

Do *NOT* migrate anything in this project that is not in the `summit-video-app/` folder. The other modules, such as `openstack/` and `summit/` have been hacked to pieces to make this work.

