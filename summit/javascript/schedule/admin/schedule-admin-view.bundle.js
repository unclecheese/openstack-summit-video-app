/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(riot) {var api               = __webpack_require__(38);
	var dispatcher        = __webpack_require__(39);
	var published_store   = __webpack_require__(40);
	var unpublished_store = __webpack_require__(41);

	__webpack_require__(42);
	__webpack_require__(45);
	__webpack_require__(46);
	__webpack_require__(47);
	__webpack_require__(48);
	__webpack_require__(49);
	__webpack_require__(50);
	__webpack_require__(51);
	__webpack_require__(52);

	riot.mount('schedule-admin-view-published-filters', { dispatcher: dispatcher});
	riot.mount('schedule-admin-view-published', { api: api, dispatcher: dispatcher, published_store: published_store, unpublished_store: unpublished_store });
	riot.mount('schedule-admin-view-published-results', { api: api, dispatcher: dispatcher, published_store: published_store });
	riot.mount('schedule-admin-view-empty-spots', { api: api, dispatcher: dispatcher, published_store: published_store });
	riot.mount('schedule-admin-view-unpublished-filters', { api: api, dispatcher: dispatcher });
	riot.mount('schedule-admin-view-unpublished', { api: api , unpublished_store : unpublished_store , dispatcher: dispatcher});
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1)))

/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_RESULT__;/* Riot v2.3.12, @license MIT, (c) 2015 Muut Inc. + contributors */

	;(function(window, undefined) {
	  'use strict';
	var riot = { version: 'v2.3.12', settings: {} },
	  // be aware, internal usage
	  // ATTENTION: prefix the global dynamic variables with `__`

	  // counter to give a unique id to all the Tag instances
	  __uid = 0,
	  // tags instances cache
	  __virtualDom = [],
	  // tags implementation cache
	  __tagImpl = {},

	  /**
	   * Const
	   */
	  // riot specific prefixes
	  RIOT_PREFIX = 'riot-',
	  RIOT_TAG = RIOT_PREFIX + 'tag',

	  // for typeof == '' comparisons
	  T_STRING = 'string',
	  T_OBJECT = 'object',
	  T_UNDEF  = 'undefined',
	  T_FUNCTION = 'function',
	  // special native tags that cannot be treated like the others
	  SPECIAL_TAGS_REGEX = /^(?:opt(ion|group)|tbody|col|t[rhd])$/,
	  RESERVED_WORDS_BLACKLIST = ['_item', '_id', '_parent', 'update', 'root', 'mount', 'unmount', 'mixin', 'isMounted', 'isLoop', 'tags', 'parent', 'opts', 'trigger', 'on', 'off', 'one'],

	  // version# for IE 8-11, 0 for others
	  IE_VERSION = (window && window.document || {}).documentMode | 0
	/* istanbul ignore next */
	riot.observable = function(el) {

	  /**
	   * Extend the original object or create a new empty one
	   * @type { Object }
	   */

	  el = el || {}

	  /**
	   * Private variables and methods
	   */

	  var callbacks = {},
	    onEachEvent = function(e, fn) { e.replace(/\S+/g, fn) },
	    defineProperty = function (key, value) {
	      Object.defineProperty(el, key, {
	        value: value,
	        enumerable: false,
	        writable: false,
	        configurable: false
	      })
	    }

	  /**
	   * Listen to the given space separated list of `events` and execute the `callback` each time an event is triggered.
	   * @param  { String } events - events ids
	   * @param  { Function } fn - callback function
	   * @returns { Object } el
	   */

	  defineProperty('on', function(events, fn) {
	    if (typeof fn != 'function')  return el

	    onEachEvent(events, function(name, pos) {
	      (callbacks[name] = callbacks[name] || []).push(fn)
	      fn.typed = pos > 0
	    })

	    return el
	  })

	  /**
	   * Removes the given space separated list of `events` listeners
	   * @param   { String } events - events ids
	   * @param   { Function } fn - callback function
	   * @returns { Object } el
	   */

	  defineProperty('off', function(events, fn) {
	    if (events == '*') callbacks = {}
	    else {
	      onEachEvent(events, function(name) {
	        if (fn) {
	          var arr = callbacks[name]
	          for (var i = 0, cb; cb = arr && arr[i]; ++i) {
	            if (cb == fn) arr.splice(i--, 1)
	          }
	        } else delete callbacks[name]
	      })
	    }
	    return el
	  })

	  /**
	   * Listen to the given space separated list of `events` and execute the `callback` at most once
	   * @param   { String } events - events ids
	   * @param   { Function } fn - callback function
	   * @returns { Object } el
	   */

	  defineProperty('one', function(events, fn) {
	    function on() {
	      el.off(events, on)
	      fn.apply(el, arguments)
	    }
	    return el.on(events, on)
	  })

	  /**
	   * Execute all callback functions that listen to the given space separated list of `events`
	   * @param   { String } events - events ids
	   * @returns { Object } el
	   */

	  defineProperty('trigger', function(events) {

	    // getting the arguments
	    // skipping the first one
	    var arglen = arguments.length - 1,
	      args = new Array(arglen)
	    for (var i = 0; i < arglen; i++) {
	      args[i] = arguments[i + 1]
	    }

	    onEachEvent(events, function(name) {

	      var fns = (callbacks[name] || []).slice(0)

	      for (var i = 0, fn; fn = fns[i]; ++i) {
	        if (fn.busy) return
	        fn.busy = 1

	        try {
	          fn.apply(el, fn.typed ? [name].concat(args) : args)
	        } catch (e) { el.trigger('error', e) }
	        if (fns[i] !== fn) { i-- }
	        fn.busy = 0
	      }

	      if (callbacks.all && name != 'all')
	        el.trigger.apply(el, ['all', name].concat(args))

	    })

	    return el
	  })

	  return el

	}
	/* istanbul ignore next */
	;(function(riot) { if (!window) return;

	/**
	 * Simple client-side router
	 * @module riot-route
	 */


	var RE_ORIGIN = /^.+?\/+[^\/]+/,
	  EVENT_LISTENER = 'EventListener',
	  REMOVE_EVENT_LISTENER = 'remove' + EVENT_LISTENER,
	  ADD_EVENT_LISTENER = 'add' + EVENT_LISTENER,
	  HAS_ATTRIBUTE = 'hasAttribute',
	  REPLACE = 'replace',
	  POPSTATE = 'popstate',
	  HASHCHANGE = 'hashchange',
	  TRIGGER = 'trigger',
	  MAX_EMIT_STACK_LEVEL = 3,
	  win = window,
	  doc = document,
	  loc = win.history.location || win.location, // see html5-history-api
	  prot = Router.prototype, // to minify more
	  clickEvent = doc && doc.ontouchstart ? 'touchstart' : 'click',
	  started = false,
	  central = riot.observable(),
	  routeFound = false,
	  debouncedEmit,
	  base, current, parser, secondParser, emitStack = [], emitStackLevel = 0

	/**
	 * Default parser. You can replace it via router.parser method.
	 * @param {string} path - current path (normalized)
	 * @returns {array} array
	 */
	function DEFAULT_PARSER(path) {
	  return path.split(/[/?#]/)
	}

	/**
	 * Default parser (second). You can replace it via router.parser method.
	 * @param {string} path - current path (normalized)
	 * @param {string} filter - filter string (normalized)
	 * @returns {array} array
	 */
	function DEFAULT_SECOND_PARSER(path, filter) {
	  var re = new RegExp('^' + filter[REPLACE](/\*/g, '([^/?#]+?)')[REPLACE](/\.\./, '.*') + '$'),
	    args = path.match(re)

	  if (args) return args.slice(1)
	}

	/**
	 * Simple/cheap debounce implementation
	 * @param   {function} fn - callback
	 * @param   {number} delay - delay in seconds
	 * @returns {function} debounced function
	 */
	function debounce(fn, delay) {
	  var t
	  return function () {
	    clearTimeout(t)
	    t = setTimeout(fn, delay)
	  }
	}

	/**
	 * Set the window listeners to trigger the routes
	 * @param {boolean} autoExec - see route.start
	 */
	function start(autoExec) {
	  debouncedEmit = debounce(emit, 1)
	  win[ADD_EVENT_LISTENER](POPSTATE, debouncedEmit)
	  win[ADD_EVENT_LISTENER](HASHCHANGE, debouncedEmit)
	  doc[ADD_EVENT_LISTENER](clickEvent, click)
	  if (autoExec) emit(true)
	}

	/**
	 * Router class
	 */
	function Router() {
	  this.$ = []
	  riot.observable(this) // make it observable
	  central.on('stop', this.s.bind(this))
	  central.on('emit', this.e.bind(this))
	}

	function normalize(path) {
	  return path[REPLACE](/^\/|\/$/, '')
	}

	function isString(str) {
	  return typeof str == 'string'
	}

	/**
	 * Get the part after domain name
	 * @param {string} href - fullpath
	 * @returns {string} path from root
	 */
	function getPathFromRoot(href) {
	  return (href || loc.href)[REPLACE](RE_ORIGIN, '')
	}

	/**
	 * Get the part after base
	 * @param {string} href - fullpath
	 * @returns {string} path from base
	 */
	function getPathFromBase(href) {
	  return base[0] == '#'
	    ? (href || loc.href).split(base)[1] || ''
	    : getPathFromRoot(href)[REPLACE](base, '')
	}

	function emit(force) {
	  // the stack is needed for redirections
	  var isRoot = emitStackLevel == 0
	  if (MAX_EMIT_STACK_LEVEL <= emitStackLevel) return

	  emitStackLevel++
	  emitStack.push(function() {
	    var path = getPathFromBase()
	    if (force || path != current) {
	      central[TRIGGER]('emit', path)
	      current = path
	    }
	  })
	  if (isRoot) {
	    while (emitStack.length) {
	      emitStack[0]()
	      emitStack.shift()
	    }
	    emitStackLevel = 0
	  }
	}

	function click(e) {
	  if (
	    e.which != 1 // not left click
	    || e.metaKey || e.ctrlKey || e.shiftKey // or meta keys
	    || e.defaultPrevented // or default prevented
	  ) return

	  var el = e.target
	  while (el && el.nodeName != 'A') el = el.parentNode
	  if (
	    !el || el.nodeName != 'A' // not A tag
	    || el[HAS_ATTRIBUTE]('download') // has download attr
	    || !el[HAS_ATTRIBUTE]('href') // has no href attr
	    || el.target && el.target != '_self' // another window or frame
	    || el.href.indexOf(loc.href.match(RE_ORIGIN)[0]) == -1 // cross origin
	  ) return

	  if (el.href != loc.href) {
	    if (
	      el.href.split('#')[0] == loc.href.split('#')[0] // internal jump
	      || base != '#' && getPathFromRoot(el.href).indexOf(base) !== 0 // outside of base
	      || !go(getPathFromBase(el.href), el.title || doc.title) // route not found
	    ) return
	  }

	  e.preventDefault()
	}

	/**
	 * Go to the path
	 * @param {string} path - destination path
	 * @param {string} title - page title
	 * @returns {boolean} - route not found flag
	 */
	function go(path, title) {
	  title = title || doc.title
	  // browsers ignores the second parameter `title`
	  history.pushState(null, title, base + normalize(path))
	  // so we need to set it manually
	  doc.title = title
	  routeFound = false
	  emit()
	  return routeFound
	}

	/**
	 * Go to path or set action
	 * a single string:                go there
	 * two strings:                    go there with setting a title
	 * a single function:              set an action on the default route
	 * a string/RegExp and a function: set an action on the route
	 * @param {(string|function)} first - path / action / filter
	 * @param {(string|RegExp|function)} second - title / action
	 */
	prot.m = function(first, second) {
	  if (isString(first) && (!second || isString(second))) go(first, second)
	  else if (second) this.r(first, second)
	  else this.r('@', first)
	}

	/**
	 * Stop routing
	 */
	prot.s = function() {
	  this.off('*')
	  this.$ = []
	}

	/**
	 * Emit
	 * @param {string} path - path
	 */
	prot.e = function(path) {
	  this.$.concat('@').some(function(filter) {
	    var args = (filter == '@' ? parser : secondParser)(normalize(path), normalize(filter))
	    if (typeof args != 'undefined') {
	      this[TRIGGER].apply(null, [filter].concat(args))
	      return routeFound = true // exit from loop
	    }
	  }, this)
	}

	/**
	 * Register route
	 * @param {string} filter - filter for matching to url
	 * @param {function} action - action to register
	 */
	prot.r = function(filter, action) {
	  if (filter != '@') {
	    filter = '/' + normalize(filter)
	    this.$.push(filter)
	  }
	  this.on(filter, action)
	}

	var mainRouter = new Router()
	var route = mainRouter.m.bind(mainRouter)

	/**
	 * Create a sub router
	 * @returns {function} the method of a new Router object
	 */
	route.create = function() {
	  var newSubRouter = new Router()
	  // stop only this sub-router
	  newSubRouter.m.stop = newSubRouter.s.bind(newSubRouter)
	  // return sub-router's main method
	  return newSubRouter.m.bind(newSubRouter)
	}

	/**
	 * Set the base of url
	 * @param {(str|RegExp)} arg - a new base or '#' or '#!'
	 */
	route.base = function(arg) {
	  base = arg || '#'
	  current = getPathFromBase() // recalculate current path
	}

	/** Exec routing right now **/
	route.exec = function() {
	  emit(true)
	}

	/**
	 * Replace the default router to yours
	 * @param {function} fn - your parser function
	 * @param {function} fn2 - your secondParser function
	 */
	route.parser = function(fn, fn2) {
	  if (!fn && !fn2) {
	    // reset parser for testing...
	    parser = DEFAULT_PARSER
	    secondParser = DEFAULT_SECOND_PARSER
	  }
	  if (fn) parser = fn
	  if (fn2) secondParser = fn2
	}

	/**
	 * Helper function to get url query as an object
	 * @returns {object} parsed query
	 */
	route.query = function() {
	  var q = {}
	  loc.href[REPLACE](/[?&](.+?)=([^&]*)/g, function(_, k, v) { q[k] = v })
	  return q
	}

	/** Stop routing **/
	route.stop = function () {
	  if (started) {
	    win[REMOVE_EVENT_LISTENER](POPSTATE, debouncedEmit)
	    win[REMOVE_EVENT_LISTENER](HASHCHANGE, debouncedEmit)
	    doc[REMOVE_EVENT_LISTENER](clickEvent, click)
	    central[TRIGGER]('stop')
	    started = false
	  }
	}

	/**
	 * Start routing
	 * @param {boolean} autoExec - automatically exec after starting if true
	 */
	route.start = function (autoExec) {
	  if (!started) {
	    if (document.readyState == 'complete') start(autoExec)
	    // the timeout is needed to solve
	    // a weird safari bug https://github.com/riot/route/issues/33
	    else win[ADD_EVENT_LISTENER]('load', function() {
	      setTimeout(function() { start(autoExec) }, 1)
	    })
	    started = true
	  }
	}

	/** Prepare the router **/
	route.base()
	route.parser()

	riot.route = route
	})(riot)
	/* istanbul ignore next */

	/**
	 * The riot template engine
	 * @version v2.3.19
	 */

	/**
	 * @module brackets
	 *
	 * `brackets         ` Returns a string or regex based on its parameter
	 * `brackets.settings` Mirrors the `riot.settings` object (use brackets.set in new code)
	 * `brackets.set     ` Change the current riot brackets
	 */

	var brackets = (function (UNDEF) {

	  var
	    REGLOB  = 'g',

	    MLCOMMS = /\/\*[^*]*\*+(?:[^*\/][^*]*\*+)*\//g,
	    STRINGS = /"[^"\\]*(?:\\[\S\s][^"\\]*)*"|'[^'\\]*(?:\\[\S\s][^'\\]*)*'/g,

	    S_QBSRC = STRINGS.source + '|' +
	      /(?:\breturn\s+|(?:[$\w\)\]]|\+\+|--)\s*(\/)(?![*\/]))/.source + '|' +
	      /\/(?=[^*\/])[^[\/\\]*(?:(?:\[(?:\\.|[^\]\\]*)*\]|\\.)[^[\/\\]*)*?(\/)[gim]*/.source,

	    DEFAULT = '{ }',

	    FINDBRACES = {
	      '(': RegExp('([()])|'   + S_QBSRC, REGLOB),
	      '[': RegExp('([[\\]])|' + S_QBSRC, REGLOB),
	      '{': RegExp('([{}])|'   + S_QBSRC, REGLOB)
	    }

	  var
	    cachedBrackets = UNDEF,
	    _regex,
	    _pairs = []

	  function _loopback(re) { return re }

	  function _rewrite(re, bp) {
	    if (!bp) bp = _pairs
	    return new RegExp(
	      re.source.replace(/{/g, bp[2]).replace(/}/g, bp[3]), re.global ? REGLOB : ''
	    )
	  }

	  function _create(pair) {
	    var
	      cvt,
	      arr = pair.split(' ')

	    if (pair === DEFAULT) {
	      arr[2] = arr[0]
	      arr[3] = arr[1]
	      cvt = _loopback
	    }
	    else {
	      if (arr.length !== 2 || /[\x00-\x1F<>a-zA-Z0-9'",;\\]/.test(pair)) {
	        throw new Error('Unsupported brackets "' + pair + '"')
	      }
	      arr = arr.concat(pair.replace(/(?=[[\]()*+?.^$|])/g, '\\').split(' '))
	      cvt = _rewrite
	    }
	    arr[4] = cvt(arr[1].length > 1 ? /{[\S\s]*?}/ : /{[^}]*}/, arr)
	    arr[5] = cvt(/\\({|})/g, arr)
	    arr[6] = cvt(/(\\?)({)/g, arr)
	    arr[7] = RegExp('(\\\\?)(?:([[({])|(' + arr[3] + '))|' + S_QBSRC, REGLOB)
	    arr[8] = pair
	    return arr
	  }

	  function _reset(pair) {
	    if (!pair) pair = DEFAULT

	    if (pair !== _pairs[8]) {
	      _pairs = _create(pair)
	      _regex = pair === DEFAULT ? _loopback : _rewrite
	      _pairs[9] = _regex(/^\s*{\^?\s*([$\w]+)(?:\s*,\s*(\S+))?\s+in\s+(\S.*)\s*}/)
	      _pairs[10] = _regex(/(^|[^\\]){=[\S\s]*?}/)
	      _brackets._rawOffset = _pairs[0].length
	    }
	    cachedBrackets = pair
	  }

	  function _brackets(reOrIdx) {
	    return reOrIdx instanceof RegExp ? _regex(reOrIdx) : _pairs[reOrIdx]
	  }

	  _brackets.split = function split(str, tmpl, _bp) {
	    // istanbul ignore next: _bp is for the compiler
	    if (!_bp) _bp = _pairs

	    var
	      parts = [],
	      match,
	      isexpr,
	      start,
	      pos,
	      re = _bp[6]

	    isexpr = start = re.lastIndex = 0

	    while (match = re.exec(str)) {

	      pos = match.index

	      if (isexpr) {

	        if (match[2]) {
	          re.lastIndex = skipBraces(match[2], re.lastIndex)
	          continue
	        }

	        if (!match[3])
	          continue
	      }

	      if (!match[1]) {
	        unescapeStr(str.slice(start, pos))
	        start = re.lastIndex
	        re = _bp[6 + (isexpr ^= 1)]
	        re.lastIndex = start
	      }
	    }

	    if (str && start < str.length) {
	      unescapeStr(str.slice(start))
	    }

	    return parts

	    function unescapeStr(str) {
	      if (tmpl || isexpr)
	        parts.push(str && str.replace(_bp[5], '$1'))
	      else
	        parts.push(str)
	    }

	    function skipBraces(ch, pos) {
	      var
	        match,
	        recch = FINDBRACES[ch],
	        level = 1
	      recch.lastIndex = pos

	      while (match = recch.exec(str)) {
	        if (match[1] &&
	          !(match[1] === ch ? ++level : --level)) break
	      }
	      return match ? recch.lastIndex : str.length
	    }
	  }

	  _brackets.hasExpr = function hasExpr(str) {
	    return _brackets(4).test(str)
	  }

	  _brackets.loopKeys = function loopKeys(expr) {
	    var m = expr.match(_brackets(9))
	    return m ?
	      { key: m[1], pos: m[2], val: _pairs[0] + m[3].trim() + _pairs[1] } : { val: expr.trim() }
	  }

	  _brackets.array = function array(pair) {
	    return _create(pair || cachedBrackets)
	  }

	  var _settings
	  function _setSettings(o) {
	    var b
	    o = o || {}
	    b = o.brackets
	    Object.defineProperty(o, 'brackets', {
	      set: _reset,
	      get: function () { return cachedBrackets },
	      enumerable: true
	    })
	    _settings = o
	    _reset(b)
	  }
	  Object.defineProperty(_brackets, 'settings', {
	    set: _setSettings,
	    get: function () { return _settings }
	  })

	  /* istanbul ignore next: in the node version riot is not in the scope */
	  _brackets.settings = typeof riot !== 'undefined' && riot.settings || {}
	  _brackets.set = _reset

	  _brackets.R_STRINGS = STRINGS
	  _brackets.R_MLCOMMS = MLCOMMS
	  _brackets.S_QBLOCKS = S_QBSRC

	  return _brackets

	})()

	/**
	 * @module tmpl
	 *
	 * tmpl          - Root function, returns the template value, render with data
	 * tmpl.hasExpr  - Test the existence of a expression inside a string
	 * tmpl.loopKeys - Get the keys for an 'each' loop (used by `_each`)
	 */

	var tmpl = (function () {

	  var _cache = {}

	  function _tmpl(str, data) {
	    if (!str) return str

	    return (_cache[str] || (_cache[str] = _create(str))).call(data, _logErr)
	  }

	  _tmpl.isRaw = function (expr) {
	    return expr[brackets._rawOffset] === "="
	  }

	  _tmpl.haveRaw = function (src) {
	    return brackets(10).test(src)
	  }

	  _tmpl.hasExpr = brackets.hasExpr

	  _tmpl.loopKeys = brackets.loopKeys

	  _tmpl.errorHandler = null

	  function _logErr(err, ctx) {

	    if (_tmpl.errorHandler) {

	      err.riotData = {
	        tagName: ctx && ctx.root && ctx.root.tagName,
	        _riot_id: ctx && ctx._riot_id  //eslint-disable-line camelcase
	      }
	      _tmpl.errorHandler(err)
	    }
	  }

	  function _create(str) {

	    var expr = _getTmpl(str)
	    if (expr.slice(0, 11) !== 'try{return ') expr = 'return ' + expr

	    return new Function('E', expr + ';')
	  }

	  var
	    RE_QBLOCK = RegExp(brackets.S_QBLOCKS, 'g'),
	    RE_QBMARK = /\x01(\d+)~/g

	  function _getTmpl(str) {
	    var
	      qstr = [],
	      expr,
	      parts = brackets.split(str.replace(/\u2057/g, '"'), 1)

	    if (parts.length > 2 || parts[0]) {
	      var i, j, list = []

	      for (i = j = 0; i < parts.length; ++i) {

	        expr = parts[i]

	        if (expr && (expr = i & 1 ?

	              _parseExpr(expr, 1, qstr) :

	              '"' + expr
	                .replace(/\\/g, '\\\\')
	                .replace(/\r\n?|\n/g, '\\n')
	                .replace(/"/g, '\\"') +
	              '"'

	          )) list[j++] = expr

	      }

	      expr = j < 2 ? list[0] :
	             '[' + list.join(',') + '].join("")'
	    }
	    else {

	      expr = _parseExpr(parts[1], 0, qstr)
	    }

	    if (qstr[0])
	      expr = expr.replace(RE_QBMARK, function (_, pos) {
	        return qstr[pos]
	          .replace(/\r/g, '\\r')
	          .replace(/\n/g, '\\n')
	      })

	    return expr
	  }

	  var
	    CS_IDENT = /^(?:(-?[_A-Za-z\xA0-\xFF][-\w\xA0-\xFF]*)|\x01(\d+)~):/,
	    RE_BRACE = /,|([[{(])|$/g

	  function _parseExpr(expr, asText, qstr) {

	    if (expr[0] === "=") expr = expr.slice(1)

	    expr = expr
	          .replace(RE_QBLOCK, function (s, div) {
	            return s.length > 2 && !div ? '\x01' + (qstr.push(s) - 1) + '~' : s
	          })
	          .replace(/\s+/g, ' ').trim()
	          .replace(/\ ?([[\({},?\.:])\ ?/g, '$1')

	    if (expr) {
	      var
	        list = [],
	        cnt = 0,
	        match

	      while (expr &&
	            (match = expr.match(CS_IDENT)) &&
	            !match.index
	        ) {
	        var
	          key,
	          jsb,
	          re = /,|([[{(])|$/g

	        expr = RegExp.rightContext
	        key  = match[2] ? qstr[match[2]].slice(1, -1).trim().replace(/\s+/g, ' ') : match[1]

	        while (jsb = (match = re.exec(expr))[1]) skipBraces(jsb, re)

	        jsb  = expr.slice(0, match.index)
	        expr = RegExp.rightContext

	        list[cnt++] = _wrapExpr(jsb, 1, key)
	      }

	      expr = !cnt ? _wrapExpr(expr, asText) :
	          cnt > 1 ? '[' + list.join(',') + '].join(" ").trim()' : list[0]
	    }
	    return expr

	    function skipBraces(jsb, re) {
	      var
	        match,
	        lv = 1,
	        ir = jsb === '(' ? /[()]/g : jsb === '[' ? /[[\]]/g : /[{}]/g

	      ir.lastIndex = re.lastIndex
	      while (match = ir.exec(expr)) {
	        if (match[0] === jsb) ++lv
	        else if (!--lv) break
	      }
	      re.lastIndex = lv ? expr.length : ir.lastIndex
	    }
	  }

	  // istanbul ignore next: not both
	  var JS_CONTEXT = '"in this?this:' + (typeof window !== 'object' ? 'global' : 'window') + ').'
	  var JS_VARNAME = /[,{][$\w]+:|(^ *|[^$\w\.])(?!(?:typeof|true|false|null|undefined|in|instanceof|is(?:Finite|NaN)|void|NaN|new|Date|RegExp|Math)(?![$\w]))([$_A-Za-z][$\w]*)/g

	  function _wrapExpr(expr, asText, key) {
	    var tb

	    expr = expr.replace(JS_VARNAME, function (match, p, mvar, pos, s) {
	      if (mvar) {
	        pos = tb ? 0 : pos + match.length

	        if (mvar !== 'this' && mvar !== 'global' && mvar !== 'window') {
	          match = p + '("' + mvar + JS_CONTEXT + mvar
	          if (pos) tb = (s = s[pos]) === '.' || s === '(' || s === '['
	        }
	        else if (pos)
	          tb = !/^(?=(\.[$\w]+))\1(?:[^.[(]|$)/.test(s.slice(pos))
	      }
	      return match
	    })

	    if (tb) {
	      expr = 'try{return ' + expr + '}catch(e){E(e,this)}'
	    }

	    if (key) {

	      expr = (tb ?
	          'function(){' + expr + '}.call(this)' : '(' + expr + ')'
	        ) + '?"' + key + '":""'
	    }
	    else if (asText) {

	      expr = 'function(v){' + (tb ?
	          expr.replace('return ', 'v=') : 'v=(' + expr + ')'
	        ) + ';return v||v===0?v:""}.call(this)'
	    }

	    return expr
	  }

	  // istanbul ignore next: compatibility fix for beta versions
	  _tmpl.parse = function (s) { return s }

	  return _tmpl

	})()

	  tmpl.version = brackets.version = 'v2.3.19'


	/*
	  lib/browser/tag/mkdom.js

	  Includes hacks needed for the Internet Explorer version 9 and bellow

	*/
	// http://kangax.github.io/compat-table/es5/#ie8
	// http://codeplanet.io/dropping-ie8/

	var mkdom = (function (checkIE) {

	  var rootEls = {
	      'tr': 'tbody',
	      'th': 'tr',
	      'td': 'tr',
	      'tbody': 'table',
	      'col': 'colgroup'
	    },
	    GENERIC = 'div'

	  checkIE = checkIE && checkIE < 10

	  // creates any dom element in a div, table, or colgroup container
	  function _mkdom(html) {

	    var match = html && html.match(/^\s*<([-\w]+)/),
	      tagName = match && match[1].toLowerCase(),
	      rootTag = rootEls[tagName] || GENERIC,
	      el = mkEl(rootTag)

	    el.stub = true

	    /* istanbul ignore next */
	    if (checkIE && tagName && (match = tagName.match(SPECIAL_TAGS_REGEX)))
	      ie9elem(el, html, tagName, !!match[1])
	    else
	      el.innerHTML = html

	    return el
	  }

	  // creates tr, th, td, option, optgroup element for IE8-9
	  /* istanbul ignore next */
	  function ie9elem(el, html, tagName, select) {

	    var div = mkEl(GENERIC),
	      tag = select ? 'select>' : 'table>',
	      child

	    div.innerHTML = '<' + tag + html + '</' + tag

	    child = $(tagName, div)
	    if (child)
	      el.appendChild(child)

	  }
	  // end ie9elem()

	  return _mkdom

	})(IE_VERSION)

	/**
	 * Convert the item looped into an object used to extend the child tag properties
	 * @param   { Object } expr - object containing the keys used to extend the children tags
	 * @param   { * } key - value to assign to the new object returned
	 * @param   { * } val - value containing the position of the item in the array
	 * @returns { Object } - new object containing the values of the original item
	 *
	 * The variables 'key' and 'val' are arbitrary.
	 * They depend on the collection type looped (Array, Object)
	 * and on the expression used on the each tag
	 *
	 */
	function mkitem(expr, key, val) {
	  var item = {}
	  item[expr.key] = key
	  if (expr.pos) item[expr.pos] = val
	  return item
	}

	/**
	 * Unmount the redundant tags
	 * @param   { Array } items - array containing the current items to loop
	 * @param   { Array } tags - array containing all the children tags
	 */
	function unmountRedundant(items, tags) {

	  var i = tags.length,
	    j = items.length

	  while (i > j) {
	    var t = tags[--i]
	    tags.splice(i, 1)
	    t.unmount()
	  }
	}

	/**
	 * Move the nested custom tags in non custom loop tags
	 * @param   { Object } child - non custom loop tag
	 * @param   { Number } i - current position of the loop tag
	 */
	function moveNestedTags(child, i) {
	  Object.keys(child.tags).forEach(function(tagName) {
	    var tag = child.tags[tagName]
	    if (isArray(tag))
	      each(tag, function (t) {
	        moveChildTag(t, tagName, i)
	      })
	    else
	      moveChildTag(tag, tagName, i)
	  })
	}

	/**
	 * Adds the elements for a virtual tag
	 * @param { Tag } tag - the tag whose root's children will be inserted or appended
	 * @param { Node } src - the node that will do the inserting or appending
	 * @param { Tag } target - only if inserting, insert before this tag's first child
	 */
	function addVirtual(tag, src, target) {
	  var el = tag._root
	  tag._virts = []
	  while (el) {
	    var sib = el.nextSibling
	    if (target)
	      src.insertBefore(el, target._root)
	    else
	      src.appendChild(el)

	    tag._virts.push(el) // hold for unmounting
	    el = sib
	  }
	}

	/**
	 * Move virtual tag and all child nodes
	 * @param { Tag } tag - first child reference used to start move
	 * @param { Node } src  - the node that will do the inserting
	 * @param { Tag } target - insert before this tag's first child
	 * @param { Number } len - how many child nodes to move
	 */
	function moveVirtual(tag, src, target, len) {
	  var el = tag._root
	  for (var i = 0; i < len; i++) {
	    var sib = el.nextSibling
	    src.insertBefore(el, target._root)
	    el = sib
	  }
	}


	/**
	 * Manage tags having the 'each'
	 * @param   { Object } dom - DOM node we need to loop
	 * @param   { Tag } parent - parent tag instance where the dom node is contained
	 * @param   { String } expr - string contained in the 'each' attribute
	 */
	function _each(dom, parent, expr) {

	  // remove the each property from the original tag
	  remAttr(dom, 'each')

	  var mustReorder = typeof getAttr(dom, 'no-reorder') !== T_STRING || remAttr(dom, 'no-reorder'),
	    tagName = getTagName(dom),
	    impl = __tagImpl[tagName] || { tmpl: dom.outerHTML },
	    useRoot = SPECIAL_TAGS_REGEX.test(tagName),
	    root = dom.parentNode,
	    ref = document.createTextNode(''),
	    child = getTag(dom),
	    isOption = /option/gi.test(tagName), // the option tags must be treated differently
	    tags = [],
	    oldItems = [],
	    hasKeys,
	    isVirtual = dom.tagName == 'VIRTUAL'

	  // parse the each expression
	  expr = tmpl.loopKeys(expr)

	  // insert a marked where the loop tags will be injected
	  root.insertBefore(ref, dom)

	  // clean template code
	  parent.one('before-mount', function () {

	    // remove the original DOM node
	    dom.parentNode.removeChild(dom)
	    if (root.stub) root = parent.root

	  }).on('update', function () {
	    // get the new items collection
	    var items = tmpl(expr.val, parent),
	      // create a fragment to hold the new DOM nodes to inject in the parent tag
	      frag = document.createDocumentFragment()



	    // object loop. any changes cause full redraw
	    if (!isArray(items)) {
	      hasKeys = items || false
	      items = hasKeys ?
	        Object.keys(items).map(function (key) {
	          return mkitem(expr, key, items[key])
	        }) : []
	    }

	    // loop all the new items
	    items.forEach(function(item, i) {
	      // reorder only if the items are objects
	      var _mustReorder = mustReorder && item instanceof Object,
	        oldPos = oldItems.indexOf(item),
	        pos = ~oldPos && _mustReorder ? oldPos : i,
	        // does a tag exist in this position?
	        tag = tags[pos]

	      item = !hasKeys && expr.key ? mkitem(expr, item, i) : item

	      // new tag
	      if (
	        !_mustReorder && !tag // with no-reorder we just update the old tags
	        ||
	        _mustReorder && !~oldPos || !tag // by default we always try to reorder the DOM elements
	      ) {

	        tag = new Tag(impl, {
	          parent: parent,
	          isLoop: true,
	          hasImpl: !!__tagImpl[tagName],
	          root: useRoot ? root : dom.cloneNode(),
	          item: item
	        }, dom.innerHTML)

	        tag.mount()
	        if (isVirtual) tag._root = tag.root.firstChild // save reference for further moves or inserts
	        // this tag must be appended
	        if (i == tags.length) {
	          if (isVirtual)
	            addVirtual(tag, frag)
	          else frag.appendChild(tag.root)
	        }
	        // this tag must be insert
	        else {
	          if (isVirtual)
	            addVirtual(tag, root, tags[i])
	          else root.insertBefore(tag.root, tags[i].root)
	          oldItems.splice(i, 0, item)
	        }

	        tags.splice(i, 0, tag)
	        pos = i // handled here so no move
	      } else tag.update(item)

	      // reorder the tag if it's not located in its previous position
	      if (pos !== i && _mustReorder) {
	        // update the DOM
	        if (isVirtual)
	          moveVirtual(tag, root, tags[i], dom.childNodes.length)
	        else root.insertBefore(tag.root, tags[i].root)
	        // update the position attribute if it exists
	        if (expr.pos)
	          tag[expr.pos] = i
	        // move the old tag instance
	        tags.splice(i, 0, tags.splice(pos, 1)[0])
	        // move the old item
	        oldItems.splice(i, 0, oldItems.splice(pos, 1)[0])
	        // if the loop tags are not custom
	        // we need to move all their custom tags into the right position
	        if (!child) moveNestedTags(tag, i)
	      }

	      // cache the original item to use it in the events bound to this node
	      // and its children
	      tag._item = item
	      // cache the real parent tag internally
	      defineProperty(tag, '_parent', parent)

	    }, true) // allow null values

	    // remove the redundant tags
	    unmountRedundant(items, tags)

	    // insert the new nodes
	    if (isOption) root.appendChild(frag)
	    else root.insertBefore(frag, ref)

	    // set the 'tags' property of the parent tag
	    // if child is 'undefined' it means that we don't need to set this property
	    // for example:
	    // we don't need store the `myTag.tags['div']` property if we are looping a div tag
	    // but we need to track the `myTag.tags['child']` property looping a custom child node named `child`
	    if (child) parent.tags[tagName] = tags

	    // clone the items array
	    oldItems = items.slice()

	  })

	}


	function parseNamedElements(root, tag, childTags, forceParsingNamed) {

	  walk(root, function(dom) {
	    if (dom.nodeType == 1) {
	      dom.isLoop = dom.isLoop || (dom.parentNode && dom.parentNode.isLoop || getAttr(dom, 'each')) ? 1 : 0

	      // custom child tag
	      if (childTags) {
	        var child = getTag(dom)

	        if (child && !dom.isLoop)
	          childTags.push(initChildTag(child, {root: dom, parent: tag}, dom.innerHTML, tag))
	      }

	      if (!dom.isLoop || forceParsingNamed)
	        setNamed(dom, tag, [])
	    }

	  })

	}

	function parseExpressions(root, tag, expressions) {

	  function addExpr(dom, val, extra) {
	    if (tmpl.hasExpr(val)) {
	      var expr = { dom: dom, expr: val }
	      expressions.push(extend(expr, extra))
	    }
	  }

	  walk(root, function(dom) {
	    var type = dom.nodeType

	    // text node
	    if (type == 3 && dom.parentNode.tagName != 'STYLE') addExpr(dom, dom.nodeValue)
	    if (type != 1) return

	    /* element */

	    // loop
	    var attr = getAttr(dom, 'each')

	    if (attr) { _each(dom, tag, attr); return false }

	    // attribute expressions
	    each(dom.attributes, function(attr) {
	      var name = attr.name,
	        bool = name.split('__')[1]

	      addExpr(dom, attr.value, { attr: bool || name, bool: bool })
	      if (bool) { remAttr(dom, name); return false }

	    })

	    // skip custom tags
	    if (getTag(dom)) return false

	  })

	}
	function Tag(impl, conf, innerHTML) {

	  var self = riot.observable(this),
	    opts = inherit(conf.opts) || {},
	    dom = mkdom(impl.tmpl),
	    parent = conf.parent,
	    isLoop = conf.isLoop,
	    hasImpl = conf.hasImpl,
	    item = cleanUpData(conf.item),
	    expressions = [],
	    childTags = [],
	    root = conf.root,
	    fn = impl.fn,
	    tagName = root.tagName.toLowerCase(),
	    attr = {},
	    propsInSyncWithParent = []

	  if (fn && root._tag) root._tag.unmount(true)

	  // not yet mounted
	  this.isMounted = false
	  root.isLoop = isLoop

	  // keep a reference to the tag just created
	  // so we will be able to mount this tag multiple times
	  root._tag = this

	  // create a unique id to this tag
	  // it could be handy to use it also to improve the virtual dom rendering speed
	  defineProperty(this, '_riot_id', ++__uid) // base 1 allows test !t._riot_id

	  extend(this, { parent: parent, root: root, opts: opts, tags: {} }, item)

	  // grab attributes
	  each(root.attributes, function(el) {
	    var val = el.value
	    // remember attributes with expressions only
	    if (tmpl.hasExpr(val)) attr[el.name] = val
	  })

	  if (dom.innerHTML && !/^(select|optgroup|table|tbody|tr|col(?:group)?)$/.test(tagName))
	    // replace all the yield tags with the tag inner html
	    dom.innerHTML = replaceYield(dom.innerHTML, innerHTML)

	  // options
	  function updateOpts() {
	    var ctx = hasImpl && isLoop ? self : parent || self

	    // update opts from current DOM attributes
	    each(root.attributes, function(el) {
	      opts[toCamel(el.name)] = tmpl(el.value, ctx)
	    })
	    // recover those with expressions
	    each(Object.keys(attr), function(name) {
	      opts[toCamel(name)] = tmpl(attr[name], ctx)
	    })
	  }

	  function normalizeData(data) {
	    for (var key in item) {
	      if (typeof self[key] !== T_UNDEF && isWritable(self, key))
	        self[key] = data[key]
	    }
	  }

	  function inheritFromParent () {
	    if (!self.parent || !isLoop) return
	    each(Object.keys(self.parent), function(k) {
	      // some properties must be always in sync with the parent tag
	      var mustSync = !contains(RESERVED_WORDS_BLACKLIST, k) && contains(propsInSyncWithParent, k)
	      if (typeof self[k] === T_UNDEF || mustSync) {
	        // track the property to keep in sync
	        // so we can keep it updated
	        if (!mustSync) propsInSyncWithParent.push(k)
	        self[k] = self.parent[k]
	      }
	    })
	  }

	  defineProperty(this, 'update', function(data) {

	    // make sure the data passed will not override
	    // the component core methods
	    data = cleanUpData(data)
	    // inherit properties from the parent
	    inheritFromParent()
	    // normalize the tag properties in case an item object was initially passed
	    if (data && typeof item === T_OBJECT) {
	      normalizeData(data)
	      item = data
	    }
	    extend(self, data)
	    updateOpts()
	    self.trigger('update', data)
	    update(expressions, self)
	    // the updated event will be triggered
	    // once the DOM will be ready and all the reflow are completed
	    // this is useful if you want to get the "real" root properties
	    // 4 ex: root.offsetWidth ...
	    rAF(function() { self.trigger('updated') })
	    return this
	  })

	  defineProperty(this, 'mixin', function() {
	    each(arguments, function(mix) {
	      var instance

	      mix = typeof mix === T_STRING ? riot.mixin(mix) : mix

	      // check if the mixin is a function
	      if (isFunction(mix)) {
	        // create the new mixin instance
	        instance = new mix()
	        // save the prototype to loop it afterwards
	        mix = mix.prototype
	      } else instance = mix

	      // loop the keys in the function prototype or the all object keys
	      each(Object.getOwnPropertyNames(mix), function(key) {
	        // bind methods to self
	        if (key != 'init')
	          self[key] = isFunction(instance[key]) ?
	                        instance[key].bind(self) :
	                        instance[key]
	      })

	      // init method will be called automatically
	      if (instance.init) instance.init.bind(self)()
	    })
	    return this
	  })

	  defineProperty(this, 'mount', function() {

	    updateOpts()

	    // initialiation
	    if (fn) fn.call(self, opts)

	    // parse layout after init. fn may calculate args for nested custom tags
	    parseExpressions(dom, self, expressions)

	    // mount the child tags
	    toggle(true)

	    // update the root adding custom attributes coming from the compiler
	    // it fixes also #1087
	    if (impl.attrs || hasImpl) {
	      walkAttributes(impl.attrs, function (k, v) { setAttr(root, k, v) })
	      parseExpressions(self.root, self, expressions)
	    }

	    if (!self.parent || isLoop) self.update(item)

	    // internal use only, fixes #403
	    self.trigger('before-mount')

	    if (isLoop && !hasImpl) {
	      // update the root attribute for the looped elements
	      self.root = root = dom.firstChild

	    } else {
	      while (dom.firstChild) root.appendChild(dom.firstChild)
	      if (root.stub) self.root = root = parent.root
	    }

	    // parse the named dom nodes in the looped child
	    // adding them to the parent as well
	    if (isLoop)
	      parseNamedElements(self.root, self.parent, null, true)

	    // if it's not a child tag we can trigger its mount event
	    if (!self.parent || self.parent.isMounted) {
	      self.isMounted = true
	      self.trigger('mount')
	    }
	    // otherwise we need to wait that the parent event gets triggered
	    else self.parent.one('mount', function() {
	      // avoid to trigger the `mount` event for the tags
	      // not visible included in an if statement
	      if (!isInStub(self.root)) {
	        self.parent.isMounted = self.isMounted = true
	        self.trigger('mount')
	      }
	    })
	  })


	  defineProperty(this, 'unmount', function(keepRootTag) {
	    var el = root,
	      p = el.parentNode,
	      ptag

	    self.trigger('before-unmount')

	    // remove this tag instance from the global virtualDom variable
	    __virtualDom.splice(__virtualDom.indexOf(self), 1)

	    if (this._virts) {
	      each(this._virts, function(v) {
	        v.parentNode.removeChild(v)
	      })
	    }

	    if (p) {

	      if (parent) {
	        ptag = getImmediateCustomParentTag(parent)
	        // remove this tag from the parent tags object
	        // if there are multiple nested tags with same name..
	        // remove this element form the array
	        if (isArray(ptag.tags[tagName]))
	          each(ptag.tags[tagName], function(tag, i) {
	            if (tag._riot_id == self._riot_id)
	              ptag.tags[tagName].splice(i, 1)
	          })
	        else
	          // otherwise just delete the tag instance
	          ptag.tags[tagName] = undefined
	      }

	      else
	        while (el.firstChild) el.removeChild(el.firstChild)

	      if (!keepRootTag)
	        p.removeChild(el)
	      else
	        // the riot-tag attribute isn't needed anymore, remove it
	        remAttr(p, 'riot-tag')
	    }


	    self.trigger('unmount')
	    toggle()
	    self.off('*')
	    self.isMounted = false
	    // somehow ie8 does not like `delete root._tag`
	    root._tag = null

	  })

	  function toggle(isMount) {

	    // mount/unmount children
	    each(childTags, function(child) { child[isMount ? 'mount' : 'unmount']() })

	    // listen/unlisten parent (events flow one way from parent to children)
	    if (parent) {
	      var evt = isMount ? 'on' : 'off'

	      // the loop tags will be always in sync with the parent automatically
	      if (isLoop)
	        parent[evt]('unmount', self.unmount)
	      else
	        parent[evt]('update', self.update)[evt]('unmount', self.unmount)
	    }
	  }

	  // named elements available for fn
	  parseNamedElements(dom, this, childTags)

	}
	/**
	 * Attach an event to a DOM node
	 * @param { String } name - event name
	 * @param { Function } handler - event callback
	 * @param { Object } dom - dom node
	 * @param { Tag } tag - tag instance
	 */
	function setEventHandler(name, handler, dom, tag) {

	  dom[name] = function(e) {

	    var ptag = tag._parent,
	      item = tag._item,
	      el

	    if (!item)
	      while (ptag && !item) {
	        item = ptag._item
	        ptag = ptag._parent
	      }

	    // cross browser event fix
	    e = e || window.event

	    // override the event properties
	    if (isWritable(e, 'currentTarget')) e.currentTarget = dom
	    if (isWritable(e, 'target')) e.target = e.srcElement
	    if (isWritable(e, 'which')) e.which = e.charCode || e.keyCode

	    e.item = item

	    // prevent default behaviour (by default)
	    if (handler.call(tag, e) !== true && !/radio|check/.test(dom.type)) {
	      if (e.preventDefault) e.preventDefault()
	      e.returnValue = false
	    }

	    if (!e.preventUpdate) {
	      el = item ? getImmediateCustomParentTag(ptag) : tag
	      el.update()
	    }

	  }

	}


	/**
	 * Insert a DOM node replacing another one (used by if- attribute)
	 * @param   { Object } root - parent node
	 * @param   { Object } node - node replaced
	 * @param   { Object } before - node added
	 */
	function insertTo(root, node, before) {
	  if (root) {
	    root.insertBefore(before, node)
	    root.removeChild(node)
	  }
	}

	/**
	 * Update the expressions in a Tag instance
	 * @param   { Array } expressions - expression that must be re evaluated
	 * @param   { Tag } tag - tag instance
	 */
	function update(expressions, tag) {

	  each(expressions, function(expr, i) {

	    var dom = expr.dom,
	      attrName = expr.attr,
	      value = tmpl(expr.expr, tag),
	      parent = expr.dom.parentNode

	    if (expr.bool)
	      value = value ? attrName : false
	    else if (value == null)
	      value = ''

	    // leave out riot- prefixes from strings inside textarea
	    // fix #815: any value -> string
	    if (parent && parent.tagName == 'TEXTAREA') {
	      value = ('' + value).replace(/riot-/g, '')
	      // change textarea's value
	      parent.value = value
	    }

	    // no change
	    if (expr.value === value) return
	    expr.value = value

	    // text node
	    if (!attrName) {
	      dom.nodeValue = '' + value    // #815 related
	      return
	    }

	    // remove original attribute
	    remAttr(dom, attrName)
	    // event handler
	    if (isFunction(value)) {
	      setEventHandler(attrName, value, dom, tag)

	    // if- conditional
	    } else if (attrName == 'if') {
	      var stub = expr.stub,
	        add = function() { insertTo(stub.parentNode, stub, dom) },
	        remove = function() { insertTo(dom.parentNode, dom, stub) }

	      // add to DOM
	      if (value) {
	        if (stub) {
	          add()
	          dom.inStub = false
	          // avoid to trigger the mount event if the tags is not visible yet
	          // maybe we can optimize this avoiding to mount the tag at all
	          if (!isInStub(dom)) {
	            walk(dom, function(el) {
	              if (el._tag && !el._tag.isMounted) el._tag.isMounted = !!el._tag.trigger('mount')
	            })
	          }
	        }
	      // remove from DOM
	      } else {
	        stub = expr.stub = stub || document.createTextNode('')
	        // if the parentNode is defined we can easily replace the tag
	        if (dom.parentNode)
	          remove()
	        // otherwise we need to wait the updated event
	        else (tag.parent || tag).one('updated', remove)

	        dom.inStub = true
	      }
	    // show / hide
	    } else if (/^(show|hide)$/.test(attrName)) {
	      if (attrName == 'hide') value = !value
	      dom.style.display = value ? '' : 'none'

	    // field value
	    } else if (attrName == 'value') {
	      dom.value = value

	    // <img src="{ expr }">
	    } else if (startsWith(attrName, RIOT_PREFIX) && attrName != RIOT_TAG) {
	      if (value)
	        setAttr(dom, attrName.slice(RIOT_PREFIX.length), value)

	    } else {
	      if (expr.bool) {
	        dom[attrName] = value
	        if (!value) return
	      }

	      if (value && value != 0 && typeof value !== T_OBJECT)
	        setAttr(dom, attrName, value)

	    }

	  })

	}
	/**
	 * Loops an array
	 * @param   { Array } els - collection of items
	 * @param   {Function} fn - callback function
	 * @returns { Array } the array looped
	 */
	function each(els, fn) {
	  for (var i = 0, len = (els || []).length, el; i < len; i++) {
	    el = els[i]
	    // return false -> remove current item during loop
	    if (el != null && fn(el, i) === false) i--
	  }
	  return els
	}

	/**
	 * Detect if the argument passed is a function
	 * @param   { * } v - whatever you want to pass to this function
	 * @returns { Boolean } -
	 */
	function isFunction(v) {
	  return typeof v === T_FUNCTION || false   // avoid IE problems
	}

	/**
	 * Remove any DOM attribute from a node
	 * @param   { Object } dom - DOM node we want to update
	 * @param   { String } name - name of the property we want to remove
	 */
	function remAttr(dom, name) {
	  dom.removeAttribute(name)
	}

	/**
	 * Convert a string containing dashes to camel case
	 * @param   { String } string - input string
	 * @returns { String } my-string -> myString
	 */
	function toCamel(string) {
	  return string.replace(/-(\w)/g, function(_, c) {
	    return c.toUpperCase()
	  })
	}

	/**
	 * Get the value of any DOM attribute on a node
	 * @param   { Object } dom - DOM node we want to parse
	 * @param   { String } name - name of the attribute we want to get
	 * @returns { String | undefined } name of the node attribute whether it exists
	 */
	function getAttr(dom, name) {
	  return dom.getAttribute(name)
	}

	/**
	 * Set any DOM attribute
	 * @param { Object } dom - DOM node we want to update
	 * @param { String } name - name of the property we want to set
	 * @param { String } val - value of the property we want to set
	 */
	function setAttr(dom, name, val) {
	  dom.setAttribute(name, val)
	}

	/**
	 * Detect the tag implementation by a DOM node
	 * @param   { Object } dom - DOM node we need to parse to get its tag implementation
	 * @returns { Object } it returns an object containing the implementation of a custom tag (template and boot function)
	 */
	function getTag(dom) {
	  return dom.tagName && __tagImpl[getAttr(dom, RIOT_TAG) || dom.tagName.toLowerCase()]
	}
	/**
	 * Add a child tag to its parent into the `tags` object
	 * @param   { Object } tag - child tag instance
	 * @param   { String } tagName - key where the new tag will be stored
	 * @param   { Object } parent - tag instance where the new child tag will be included
	 */
	function addChildTag(tag, tagName, parent) {
	  var cachedTag = parent.tags[tagName]

	  // if there are multiple children tags having the same name
	  if (cachedTag) {
	    // if the parent tags property is not yet an array
	    // create it adding the first cached tag
	    if (!isArray(cachedTag))
	      // don't add the same tag twice
	      if (cachedTag !== tag)
	        parent.tags[tagName] = [cachedTag]
	    // add the new nested tag to the array
	    if (!contains(parent.tags[tagName], tag))
	      parent.tags[tagName].push(tag)
	  } else {
	    parent.tags[tagName] = tag
	  }
	}

	/**
	 * Move the position of a custom tag in its parent tag
	 * @param   { Object } tag - child tag instance
	 * @param   { String } tagName - key where the tag was stored
	 * @param   { Number } newPos - index where the new tag will be stored
	 */
	function moveChildTag(tag, tagName, newPos) {
	  var parent = tag.parent,
	    tags
	  // no parent no move
	  if (!parent) return

	  tags = parent.tags[tagName]

	  if (isArray(tags))
	    tags.splice(newPos, 0, tags.splice(tags.indexOf(tag), 1)[0])
	  else addChildTag(tag, tagName, parent)
	}

	/**
	 * Create a new child tag including it correctly into its parent
	 * @param   { Object } child - child tag implementation
	 * @param   { Object } opts - tag options containing the DOM node where the tag will be mounted
	 * @param   { String } innerHTML - inner html of the child node
	 * @param   { Object } parent - instance of the parent tag including the child custom tag
	 * @returns { Object } instance of the new child tag just created
	 */
	function initChildTag(child, opts, innerHTML, parent) {
	  var tag = new Tag(child, opts, innerHTML),
	    tagName = getTagName(opts.root),
	    ptag = getImmediateCustomParentTag(parent)
	  // fix for the parent attribute in the looped elements
	  tag.parent = ptag
	  // store the real parent tag
	  // in some cases this could be different from the custom parent tag
	  // for example in nested loops
	  tag._parent = parent

	  // add this tag to the custom parent tag
	  addChildTag(tag, tagName, ptag)
	  // and also to the real parent tag
	  if (ptag !== parent)
	    addChildTag(tag, tagName, parent)
	  // empty the child node once we got its template
	  // to avoid that its children get compiled multiple times
	  opts.root.innerHTML = ''

	  return tag
	}

	/**
	 * Loop backward all the parents tree to detect the first custom parent tag
	 * @param   { Object } tag - a Tag instance
	 * @returns { Object } the instance of the first custom parent tag found
	 */
	function getImmediateCustomParentTag(tag) {
	  var ptag = tag
	  while (!getTag(ptag.root)) {
	    if (!ptag.parent) break
	    ptag = ptag.parent
	  }
	  return ptag
	}

	/**
	 * Helper function to set an immutable property
	 * @param   { Object } el - object where the new property will be set
	 * @param   { String } key - object key where the new property will be stored
	 * @param   { * } value - value of the new property
	* @param   { Object } options - set the propery overriding the default options
	 * @returns { Object } - the initial object
	 */
	function defineProperty(el, key, value, options) {
	  Object.defineProperty(el, key, extend({
	    value: value,
	    enumerable: false,
	    writable: false,
	    configurable: false
	  }, options))
	  return el
	}

	/**
	 * Get the tag name of any DOM node
	 * @param   { Object } dom - DOM node we want to parse
	 * @returns { String } name to identify this dom node in riot
	 */
	function getTagName(dom) {
	  var child = getTag(dom),
	    namedTag = getAttr(dom, 'name'),
	    tagName = namedTag && !tmpl.hasExpr(namedTag) ?
	                namedTag :
	              child ? child.name : dom.tagName.toLowerCase()

	  return tagName
	}

	/**
	 * Extend any object with other properties
	 * @param   { Object } src - source object
	 * @returns { Object } the resulting extended object
	 *
	 * var obj = { foo: 'baz' }
	 * extend(obj, {bar: 'bar', foo: 'bar'})
	 * console.log(obj) => {bar: 'bar', foo: 'bar'}
	 *
	 */
	function extend(src) {
	  var obj, args = arguments
	  for (var i = 1; i < args.length; ++i) {
	    if (obj = args[i]) {
	      for (var key in obj) {
	        // check if this property of the source object could be overridden
	        if (isWritable(src, key))
	          src[key] = obj[key]
	      }
	    }
	  }
	  return src
	}

	/**
	 * Check whether an array contains an item
	 * @param   { Array } arr - target array
	 * @param   { * } item - item to test
	 * @returns { Boolean } Does 'arr' contain 'item'?
	 */
	function contains(arr, item) {
	  return ~arr.indexOf(item)
	}

	/**
	 * Check whether an object is a kind of array
	 * @param   { * } a - anything
	 * @returns {Boolean} is 'a' an array?
	 */
	function isArray(a) { return Array.isArray(a) || a instanceof Array }

	/**
	 * Detect whether a property of an object could be overridden
	 * @param   { Object }  obj - source object
	 * @param   { String }  key - object property
	 * @returns { Boolean } is this property writable?
	 */
	function isWritable(obj, key) {
	  var props = Object.getOwnPropertyDescriptor(obj, key)
	  return typeof obj[key] === T_UNDEF || props && props.writable
	}


	/**
	 * With this function we avoid that the internal Tag methods get overridden
	 * @param   { Object } data - options we want to use to extend the tag instance
	 * @returns { Object } clean object without containing the riot internal reserved words
	 */
	function cleanUpData(data) {
	  if (!(data instanceof Tag) && !(data && typeof data.trigger == T_FUNCTION)) return data

	  var o = {}
	  for (var key in data) {
	    if (!contains(RESERVED_WORDS_BLACKLIST, key))
	      o[key] = data[key]
	  }
	  return o
	}

	/**
	 * Walk down recursively all the children tags starting dom node
	 * @param   { Object }   dom - starting node where we will start the recursion
	 * @param   { Function } fn - callback to transform the child node just found
	 */
	function walk(dom, fn) {
	  if (dom) {
	    // stop the recursion
	    if (fn(dom) === false) return
	    else {
	      dom = dom.firstChild

	      while (dom) {
	        walk(dom, fn)
	        dom = dom.nextSibling
	      }
	    }
	  }
	}

	/**
	 * Minimize risk: only zero or one _space_ between attr & value
	 * @param   { String }   html - html string we want to parse
	 * @param   { Function } fn - callback function to apply on any attribute found
	 */
	function walkAttributes(html, fn) {
	  var m,
	    re = /([-\w]+) ?= ?(?:"([^"]*)|'([^']*)|({[^}]*}))/g

	  while (m = re.exec(html)) {
	    fn(m[1].toLowerCase(), m[2] || m[3] || m[4])
	  }
	}

	/**
	 * Check whether a DOM node is in stub mode, useful for the riot 'if' directive
	 * @param   { Object }  dom - DOM node we want to parse
	 * @returns { Boolean } -
	 */
	function isInStub(dom) {
	  while (dom) {
	    if (dom.inStub) return true
	    dom = dom.parentNode
	  }
	  return false
	}

	/**
	 * Create a generic DOM node
	 * @param   { String } name - name of the DOM node we want to create
	 * @returns { Object } DOM node just created
	 */
	function mkEl(name) {
	  return document.createElement(name)
	}

	/**
	 * Create a generic DOM node, and fill it with innerHTML
	 * @param   { String } name - name of the DOM node we want to create
	 * @param   { String } innerHTML - innerHTML of the new DOM
	 * @returns { Object } DOM node just created
	 */
	function mkElWithInnerHTML(name, innerHTML) {
	  var el = mkEl(name)
	  el.innerHTML = innerHTML || ''
	  return el
	}

	/**
	 * Replace the yield tag from any tag template with the innerHTML of the
	 * original tag in the page
	 * @param   { String } tmpl - tag implementation template
	 * @param   { String } innerHTML - original content of the tag in the DOM
	 * @returns { String } tag template updated without the yield tag
	 */
	function replaceYield(tmpl, innerHTML) {
	  var tmplElement = mkElWithInnerHTML('div', tmpl)
	  // if ($('yield[from]'.tmplElement)) { // this issues test errors
	  if (tmplElement.querySelector && tmplElement.querySelector('yield[from]')) { // code coverage path not taken (?)
	    // yield to(s) must be direct children from innerHTML(root), all other tags are ignored
	    each(mkElWithInnerHTML('div', innerHTML).childNodes, function(toYield) {
	      if (toYield.nodeType == 1 && toYield.tagName == 'YIELD' && toYield.getAttribute('to')) {
	        // replace all yield[from]
	        each($$('yield[from="'+toYield.getAttribute('to')+'"]', tmplElement), function(fromYield) {
	          fromYield.outerHTML = toYield.innerHTML
	        })
	      }
	    })
	    return tmplElement.innerHTML
	  } else
	    // just replace yield in tmpl with the innerHTML
	    return tmpl.replace(/<yield\s*(?:\/>|>\s*<\/yield\s*>)/gi, innerHTML || '')
	}

	/**
	 * Shorter and fast way to select multiple nodes in the DOM
	 * @param   { String } selector - DOM selector
	 * @param   { Object } ctx - DOM node where the targets of our search will is located
	 * @returns { Object } dom nodes found
	 */
	function $$(selector, ctx) {
	  return (ctx || document).querySelectorAll(selector)
	}

	/**
	 * Shorter and fast way to select a single node in the DOM
	 * @param   { String } selector - unique dom selector
	 * @param   { Object } ctx - DOM node where the target of our search will is located
	 * @returns { Object } dom node found
	 */
	function $(selector, ctx) {
	  return (ctx || document).querySelector(selector)
	}

	/**
	 * Simple object prototypal inheritance
	 * @param   { Object } parent - parent object
	 * @returns { Object } child instance
	 */
	function inherit(parent) {
	  function Child() {}
	  Child.prototype = parent
	  return new Child()
	}

	/**
	 * Get the name property needed to identify a DOM node in riot
	 * @param   { Object } dom - DOM node we need to parse
	 * @returns { String | undefined } give us back a string to identify this dom node
	 */
	function getNamedKey(dom) {
	  return getAttr(dom, 'id') || getAttr(dom, 'name')
	}

	/**
	 * Set the named properties of a tag element
	 * @param { Object } dom - DOM node we need to parse
	 * @param { Object } parent - tag instance where the named dom element will be eventually added
	 * @param { Array } keys - list of all the tag instance properties
	 */
	function setNamed(dom, parent, keys) {
	  // get the key value we want to add to the tag instance
	  var key = getNamedKey(dom),
	    // add the node detected to a tag instance using the named property
	    add = function(value) {
	      // avoid to override the tag properties already set
	      if (contains(keys, key)) return
	      // check whether this value is an array
	      var isArr = isArray(value)
	      // if the key was never set
	      if (!value)
	        // set it once on the tag instance
	        parent[key] = dom
	      // if it was an array and not yet set
	      else if (!isArr || isArr && !contains(value, dom)) {
	        // add the dom node into the array
	        if (isArr)
	          value.push(dom)
	        else
	          parent[key] = [value, dom]
	      }
	    }

	  // skip the elements with no named properties
	  if (!key) return

	  // check whether this key has been already evaluated
	  if (tmpl.hasExpr(key))
	    // wait the first updated event only once
	    parent.one('updated', function() {
	      key = getNamedKey(dom)
	      add(parent[key])
	    })
	  else
	    add(parent[key])

	}

	/**
	 * Faster String startsWith alternative
	 * @param   { String } src - source string
	 * @param   { String } str - test string
	 * @returns { Boolean } -
	 */
	function startsWith(src, str) {
	  return src.slice(0, str.length) === str
	}

	/**
	 * Function needed to inject in runtime the custom tags css
	 */
	var injectStyle = (function() {

	  if (!window) return // skip injection on the server

	  // create the style node
	  var styleNode = mkEl('style'),
	    placeholder = $('style[type=riot]')

	  setAttr(styleNode, 'type', 'text/css')

	  // inject the new node into the DOM -- in head
	  if (placeholder) {
	    placeholder.parentNode.replaceChild(styleNode, placeholder)
	    placeholder = null
	  }
	  else document.getElementsByTagName('head')[0].appendChild(styleNode)

	  /**
	   * This is the function exported that will be used to update the style tag just created
	   * innerHTML seems slow: http://jsperf.com/riot-insert-style
	   * @param   { String } css [description]
	   */
	  return styleNode.styleSheet ?
	    function (css) { styleNode.styleSheet.cssText += css } :
	    function (css) { styleNode.innerHTML += css }

	})()

	/**
	 * requestAnimationFrame polyfill
	 */
	var rAF = (function(w) {
	  return  w.requestAnimationFrame       ||
	          w.webkitRequestAnimationFrame ||
	          w.mozRequestAnimationFrame    ||
	          function(cb) { setTimeout(cb, 1000 / 60) }
	})(window || {})

	/**
	 * Mount a tag creating new Tag instance
	 * @param   { Object } root - dom node where the tag will be mounted
	 * @param   { String } tagName - name of the riot tag we want to mount
	 * @param   { Object } opts - options to pass to the Tag instance
	 * @returns { Tag } a new Tag instance
	 */
	function mountTo(root, tagName, opts) {
	  var tag = __tagImpl[tagName],
	    // cache the inner HTML to fix #855
	    innerHTML = root._innerHTML = root._innerHTML || root.innerHTML

	  // clear the inner html
	  root.innerHTML = ''

	  if (tag && root) tag = new Tag(tag, { root: root, opts: opts }, innerHTML)

	  if (tag && tag.mount) {
	    tag.mount()
	    // add this tag to the virtualDom variable
	    if (!contains(__virtualDom, tag)) __virtualDom.push(tag)
	  }

	  return tag
	}
	/**
	 * Riot public api
	 */

	// share methods for other riot parts, e.g. compiler
	riot.util = { brackets: brackets, tmpl: tmpl }

	/**
	 * Create a mixin that could be globally shared across all the tags
	 */
	riot.mixin = (function() {
	  var mixins = {}

	  /**
	   * Create/Return a mixin by its name
	   * @param   { String } name - mixin name
	   * @param   { Object } mixin - mixin logic
	   * @returns { Object } the mixin logic
	   */
	  return function(name, mixin) {
	    if (!mixin) return mixins[name]
	    mixins[name] = mixin
	  }

	})()

	/**
	 * Create a new riot tag implementation
	 * @param   { String }   name - name/id of the new riot tag
	 * @param   { String }   html - tag template
	 * @param   { String }   css - custom tag css
	 * @param   { String }   attrs - root tag attributes
	 * @param   { Function } fn - user function
	 * @returns { String } name/id of the tag just created
	 */
	riot.tag = function(name, html, css, attrs, fn) {
	  if (isFunction(attrs)) {
	    fn = attrs
	    if (/^[\w\-]+\s?=/.test(css)) {
	      attrs = css
	      css = ''
	    } else attrs = ''
	  }
	  if (css) {
	    if (isFunction(css)) fn = css
	    else if (injectStyle) injectStyle(css)
	  }
	  __tagImpl[name] = { name: name, tmpl: html, attrs: attrs, fn: fn }
	  return name
	}

	/**
	 * Create a new riot tag implementation (for use by the compiler)
	 * @param   { String }   name - name/id of the new riot tag
	 * @param   { String }   html - tag template
	 * @param   { String }   css - custom tag css
	 * @param   { String }   attrs - root tag attributes
	 * @param   { Function } fn - user function
	 * @param   { string }  [bpair] - brackets used in the compilation
	 * @returns { String } name/id of the tag just created
	 */
	riot.tag2 = function(name, html, css, attrs, fn, bpair) {
	  if (css && injectStyle) injectStyle(css)
	  //if (bpair) riot.settings.brackets = bpair
	  __tagImpl[name] = { name: name, tmpl: html, attrs: attrs, fn: fn }
	  return name
	}

	/**
	 * Mount a tag using a specific tag implementation
	 * @param   { String } selector - tag DOM selector
	 * @param   { String } tagName - tag implementation name
	 * @param   { Object } opts - tag logic
	 * @returns { Array } new tags instances
	 */
	riot.mount = function(selector, tagName, opts) {

	  var els,
	    allTags,
	    tags = []

	  // helper functions

	  function addRiotTags(arr) {
	    var list = ''
	    each(arr, function (e) {
	      list += ', *[' + RIOT_TAG + '="' + e.trim() + '"]'
	    })
	    return list
	  }

	  function selectAllTags() {
	    var keys = Object.keys(__tagImpl)
	    return keys + addRiotTags(keys)
	  }

	  function pushTags(root) {
	    var last

	    if (root.tagName) {
	      if (tagName && (!(last = getAttr(root, RIOT_TAG)) || last != tagName))
	        setAttr(root, RIOT_TAG, tagName)

	      var tag = mountTo(root, tagName || root.getAttribute(RIOT_TAG) || root.tagName.toLowerCase(), opts)

	      if (tag) tags.push(tag)
	    } else if (root.length)
	      each(root, pushTags)   // assume nodeList

	  }

	  // ----- mount code -----

	  if (typeof tagName === T_OBJECT) {
	    opts = tagName
	    tagName = 0
	  }

	  // crawl the DOM to find the tag
	  if (typeof selector === T_STRING) {
	    if (selector === '*')
	      // select all the tags registered
	      // and also the tags found with the riot-tag attribute set
	      selector = allTags = selectAllTags()
	    else
	      // or just the ones named like the selector
	      selector += addRiotTags(selector.split(','))

	    // make sure to pass always a selector
	    // to the querySelectorAll function
	    els = selector ? $$(selector) : []
	  }
	  else
	    // probably you have passed already a tag or a NodeList
	    els = selector

	  // select all the registered and mount them inside their root elements
	  if (tagName === '*') {
	    // get all custom tags
	    tagName = allTags || selectAllTags()
	    // if the root els it's just a single tag
	    if (els.tagName)
	      els = $$(tagName, els)
	    else {
	      // select all the children for all the different root elements
	      var nodeList = []
	      each(els, function (_el) {
	        nodeList.push($$(tagName, _el))
	      })
	      els = nodeList
	    }
	    // get rid of the tagName
	    tagName = 0
	  }

	  if (els.tagName)
	    pushTags(els)
	  else
	    each(els, pushTags)

	  return tags
	}

	/**
	 * Update all the tags instances created
	 * @returns { Array } all the tags instances
	 */
	riot.update = function() {
	  return each(__virtualDom, function(tag) {
	    tag.update()
	  })
	}

	/**
	 * Export the Tag constructor
	 */
	riot.Tag = Tag
	  // support CommonJS, AMD & browser
	  /* istanbul ignore next */
	  if (typeof exports === T_OBJECT)
	    module.exports = riot
	  else if ("function" === T_FUNCTION && typeof __webpack_require__(2) !== T_UNDEF)
	    !(__WEBPACK_AMD_DEFINE_RESULT__ = function() { return (window.riot = riot) }.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__))
	  else
	    window.riot = riot

	})(typeof window != 'undefined' ? window : void 0);


/***/ },
/* 2 */
/***/ function(module, exports) {

	/* WEBPACK VAR INJECTION */(function(__webpack_amd_options__) {module.exports = __webpack_amd_options__;

	/* WEBPACK VAR INJECTION */}.call(exports, {}))

/***/ },
/* 3 */,
/* 4 */,
/* 5 */,
/* 6 */,
/* 7 */,
/* 8 */,
/* 9 */,
/* 10 */,
/* 11 */,
/* 12 */,
/* 13 */,
/* 14 */,
/* 15 */,
/* 16 */,
/* 17 */,
/* 18 */,
/* 19 */,
/* 20 */,
/* 21 */,
/* 22 */,
/* 23 */,
/* 24 */,
/* 25 */,
/* 26 */,
/* 27 */,
/* 28 */,
/* 29 */,
/* 30 */,
/* 31 */,
/* 32 */,
/* 33 */,
/* 34 */,
/* 35 */,
/* 36 */,
/* 37 */,
/* 38 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(riot) {var schedule_admin_view_api                          = riot.observable();
	var api_base_url                                     = 'api/v1/summits/@SUMMIT_ID';
	var dispatcher                                       = __webpack_require__(39);
	schedule_admin_view_api.RETRIEVED_PUBLISHED_EVENTS   = 'RETRIEVED_PUBLISHED_EVENTS';
	schedule_admin_view_api.RETRIEVED_UNPUBLISHED_EVENTS = 'RETRIEVED_UNPUBLISHED_EVENTS';
	schedule_admin_view_api.RETRIEVED_PUBLISHED_SEARCH   = 'RETRIEVED_PUBLISHED_SEARCH';
	schedule_admin_view_api.RETRIEVED_EMPTY_SPOTS        = 'RETRIEVED_EMPTY_SPOTS';

	schedule_admin_view_api.getScheduleByDayAndLocation = function (summit_id, day, location_id)
	{
	    var url = api_base_url.replace('@SUMMIT_ID', summit_id)+'/schedule?day='+day+'&location='+location_id;
	    return $.get(url,function (data) {
	        data.summit_id   = summit_id;
	        data.location_id = location_id;
	        data.day         = day;

	        schedule_admin_view_api.trigger(schedule_admin_view_api.RETRIEVED_PUBLISHED_EVENTS, data);
	    });
	}

	schedule_admin_view_api.getUnpublishedEventsBySource = function (summit_id, source, second_source, search_term, order, page, page_size)
	{
	    var url    = api_base_url.replace('@SUMMIT_ID', summit_id)+'/events/unpublished/'+source;
	    var params = { 'expand' : 'speakers'};

	    if(source === 'tracks' && second_source !== '' && typeof second_source !== 'undefined' )
	        params['track_list_id'] = second_source;
	    if(source === 'events' && second_source !== '' && typeof second_source !== 'undefined' )
	        params['event_type_id'] = second_source;
	    if(page !== '' && typeof page !== 'undefined')
	        params['page'] = page;
	    if(page_size !== '' && typeof page_size !== 'undefined')
	        params['page_size'] = page_size;
	    if(search_term !== '' && typeof search_term !== 'undefined')
	        params['search_term'] = search_term;
	    if(order !== '' && typeof order !== 'undefined')
	        params['order'] = order;

	    var query = '';
	    for(var key in params)
	    {
	        if(query !== '') query += '&';
	        query += key +'='+params[key];
	    }

	    if(query !=='' ) url += '?' + query;

	    return $.get(url,function (data) {
	        schedule_admin_view_api.trigger(schedule_admin_view_api.RETRIEVED_UNPUBLISHED_EVENTS, data);
	    });
	}

	schedule_admin_view_api.getScheduleSearchResults = function (summit_id, term)
	{
	    var url = api_base_url.replace('@SUMMIT_ID', summit_id)+'/schedule/search?term='+term;
	    return $.get(url,function (data) {
	        data.summit_id   = summit_id;
	        schedule_admin_view_api.trigger(schedule_admin_view_api.RETRIEVED_PUBLISHED_SEARCH, data);
	    });
	}

	schedule_admin_view_api.getScheduleEmptySpots = function (summit_id, days, start_time, end_time, locations, length)
	{

	    var url = api_base_url.replace('@SUMMIT_ID', summit_id)+'/schedule/empty_spots';
	    var params = {days:JSON.stringify(days),start_time:start_time,end_time:end_time,locations:JSON.stringify(locations),length:length};
	    return $.get(url,params,function (data) {
	        data.summit_id   = summit_id;
	        schedule_admin_view_api.trigger(schedule_admin_view_api.RETRIEVED_EMPTY_SPOTS, data);
	    });
	}

	schedule_admin_view_api.publish = function (summit_id, event, is_published_event)
	{
	    var url              = api_base_url.replace('@SUMMIT_ID', summit_id)+'/events/'+event.id+'/publish';
	    var clone            = jQuery.extend(true, {}, event);

	    clone.start_datetime = clone.start_datetime.format('YYYY-MM-DD HH:mm:ss');
	    clone.end_datetime   = clone.end_datetime.format('YYYY-MM-DD HH:mm:ss');
	    console.log(' start_datetime ' +clone.start_datetime+' end_datetime '+clone.end_datetime);

	    $.ajax({
	        url : url,
	        type: 'PUT',
	        data: JSON.stringify(clone),
	        contentType: "application/json; charset=utf-8"
	    })
	    .done(function() {
	    })
	    .fail(function(jqXHR) {
	        var responseCode = jqXHR.status;
	        if(responseCode == 412) {
	            var response = $.parseJSON(jqXHR.responseText);

	            swal({
	                title: 'Validation error',
	                text: response.messages[0],
	                type: 'warning'
	            },
	                function(){
	                    if (is_published_event) {
	                        $('#event_'+event.id).animate({
	                            top: $('#event_'+event.id).attr('prev-pos-top'),
	                            height: $('#event_'+event.id).attr('prev-height')
	                        });
	                    } else {
	                        $('.unpublished-events-refresh').click();
	                        $('#event_'+event.id).remove();
	                    }

	                    return false;
	                }
	            );

	            return;
	        }
	        alert( "There was an error on publishing process, please contact your administrator." );
	    });
	}

	schedule_admin_view_api.unpublish = function (summit_id, event_id){
	    var url = api_base_url.replace('@SUMMIT_ID', summit_id)+'/events/'+event_id+'/unpublish';
	    console.log('unpublish summit_id '+summit_id+' event_id '+event_id);
	    $.ajax({
	            url : url,
	            type: 'DELETE',
	            contentType: "application/json; charset=utf-8",
	            dataType: "json",
	        })
	        .done(function() {
	        })
	        .fail(function() {
	            alert( "There was an error on unpublishing process, please contact your administrator." );
	        });
	}

	dispatcher.on(dispatcher.PUBLISHED_EVENTS_FILTER_CHANGE, function(summit_id ,day , location_id){
	    schedule_admin_view_api.getScheduleByDayAndLocation(summit_id ,day , location_id);
	});

	dispatcher.on(dispatcher.PUBLISHED_EVENTS_SEARCH, function(summit_id ,term){
	    schedule_admin_view_api.getScheduleSearchResults(summit_id ,term);
	});

	dispatcher.on(dispatcher.PUBLISHED_EVENTS_SEARCH_EMPTY, function(summit_id, days, start_time, end_time, locations, length){
	    schedule_admin_view_api.getScheduleEmptySpots(summit_id, days, start_time, end_time, locations, length);
	});

	dispatcher.on(dispatcher.UNPUBLISHED_EVENT, function(summit_id, event_id){
	    schedule_admin_view_api.unpublish(summit_id, event_id);
	});

	module.exports = schedule_admin_view_api;
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1)))

/***/ },
/* 39 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(riot) {function ScheduleAdminViewDispatcher() {

	    riot.observable(this);

	    this.PUBLISHED_EVENT                   = 'PUBLISHED_EVENT';
	    this.PUBLISHED_EVENTS_FILTER_CHANGE    = 'PUBLISHED_EVENTS_FILTER_CHANGE';
	    this.PUBLISHED_EVENTS_SEARCH           = 'PUBLISHED_EVENTS_SEARCH';
	    this.PUBLISHED_EVENTS_SEARCH_EMPTY     = 'PUBLISHED_EVENTS_SEARCH_EMPTY';
	    this.PUBLISHED_EVENTS_DEEP_LINK        = 'PUBLISHED_EVENTS_DEEP_LINK';
	    this.UNPUBLISHED_EVENT                 = 'UNPUBLISHED_EVENT';
	    this.UNPUBLISHED_EVENTS_PAGE_CHANGED   = 'UNPUBLISHED_EVENTS_PAGE_CHANGED';
	    this.UNPUBLISHED_EVENTS_SOURCE_CHANGED = 'UNPUBLISHED_EVENTS_SOURCE_CHANGED';

	    this.publishEvent = function(event_id)
	    {
	        this.trigger(this.PUBLISHED_EVENT, event_id);
	    }

	    this.unPublishEvent = function(summit_id, event_id)
	    {
	        this.trigger(this.UNPUBLISHED_EVENT, summit_id, event_id);
	    }

	    this.unpublishedEventsPageChanged = function (page_nbr)
	    {
	        this.trigger(this.UNPUBLISHED_EVENTS_PAGE_CHANGED, page_nbr);
	    }

	    this.unpublishedEventsSourceChanged = function(source){
	        this.trigger(this.UNPUBLISHED_EVENTS_SOURCE_CHANGED, source);
	    }

	    this.publishedEventsFilterChanged = function(summit_id, day ,location_id)
	    {
	        this.trigger(this.PUBLISHED_EVENTS_FILTER_CHANGE,summit_id ,day , location_id);
	    }

	    this.publishedEventsSearch = function(summit_id, term)
	    {
	        this.trigger(this.PUBLISHED_EVENTS_SEARCH,summit_id ,term);
	    }

	    this.publishedEventsSearchEmpty = function(summit_id, days, start_time, end_time, locations, length)
	    {
	        this.trigger(this.PUBLISHED_EVENTS_SEARCH_EMPTY,summit_id, days, start_time, end_time, locations, length);
	    }

	    this.publishedEventsDeepLink = function ()
	    {
	        this.trigger(this.PUBLISHED_EVENTS_DEEP_LINK);
	    }
	}

	var dispatcher = new ScheduleAdminViewDispatcher();

	module.exports = dispatcher;
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1)))

/***/ },
/* 40 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(riot) {var api        = __webpack_require__(38);
	var dispatcher = __webpack_require__(39);

	function publishedEventsStore(){
	    riot.observable(this);

	    this.LOAD_STORE       = 'PUBLISHED_EVENTS_STORE_LOADED';
	    this.LOAD_RESULTS     = 'PUBLISHED_EVENTS_RESULTS_LOADED';
	    this.LOAD_EMPTY_SPOTS = 'PUBLISHED_EMPTY_SPOTS_LOADED';

	    this._published_events  = {};
	    this._published_results = {};
	    this._empty_spots       = {};
	    this._summit_id         = null;
	    this._location_id       = null;
	    this._day               = null;

	    this.currentLocation = function(){
	        return this._location_id;
	    }

	    this.currentDay = function()
	    {
	        return this._day;
	    }

	    this.clear = function() {
	        this._published_events = {};
	        this._published_results = {};
	        this._empty_spots = {};
	    }

	    this.all = function(){
	        return this._published_events;
	    }

	    this.results = function(){
	        return this._published_results;
	    }

	    this.empty_spots = function(){
	        return this._empty_spots;
	    }

	    this.add = function(event)
	    {
	        this._published_events[event.id] = event;
	    }

	    this.get = function(event_id) {
	        return this._published_events[event_id];
	    }

	    this.delete = function(event_id)
	    {
	        var item = this.get(event_id);
	        delete this._published_events[event_id];
	        self.trigger(self.LOAD_STORE);
	        return item;
	    }

	    this._load = function(events) {
	        this.clear();
	        // update model

	        for(var e of events) {
	            this._published_events[e.id] = e;
	            e.start_datetime = moment(e.start_datetime, 'YYYY-MM-DD HH:mm:ss');
	            e.end_datetime   = moment(e.end_datetime, 'YYYY-MM-DD HH:mm:ss');
	        }
	    }

	    this._load_results = function(events) {
	        this.clear();
	        // update model

	        for(var e of events) {
	            this._published_results[e.id] = e;
	        }
	    }

	    this._load_empty_spots = function(spots) {
	        this.clear();
	        // update model

	        for(var s of spots) {
	            if(!(s.location_id in this._empty_spots)) this._empty_spots[s.location_id] = [];
	            this._empty_spots[s.location_id].push(s);
	        }
	    }

	    var self = this;

	    api.on(api.RETRIEVED_PUBLISHED_EVENTS,function(response) {
	        console.log(api.RETRIEVED_PUBLISHED_EVENTS);

	        self._summit_id   = response.summit_id;
	        self._location_id = response.location_id;
	        self._day         = response.day;
	        self._load(response.events);
	        self.trigger(self.LOAD_STORE);
	    });

	    api.on(api.RETRIEVED_PUBLISHED_SEARCH,function(response) {
	        console.log(api.RETRIEVED_PUBLISHED_SEARCH);

	        self._summit_id   = response.summit_id;
	        self._load_results(response.events);
	        self.trigger(self.LOAD_RESULTS);
	    });

	    api.on(api.RETRIEVED_EMPTY_SPOTS,function(response) {
	        console.log(api.RETRIEVED_EMPTY_SPOTS);

	        self._summit_id   = response.summit_id;
	        self._load_empty_spots(response.empty_spots);
	        self.trigger(self.LOAD_EMPTY_SPOTS);
	    });
	}


	var store = new publishedEventsStore();

	dispatcher.on(dispatcher.UNPUBLISHED_EVENT, function(summit_id, event_id){
	    store.delete(event_id);
	});

	module.exports = store;
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1)))

/***/ },
/* 41 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(riot) {
	var api  = __webpack_require__(38);

	function unpublishedEventsStore(){

	    riot.observable(this);

	    this.LOAD_STORE = 'UNPUBLISHED_EVENTS_STORE_LOADED';

	    this._unpublished_events = {};
	    this._page               = 1;
	    this._page_size          = 10;
	    this._total_pages        = null

	    this.clear = function() {
	        this._unpublished_events = {};
	    }

	    this.all = function(){
	        return this._unpublished_events;
	    }

	    this.get = function(event_id) {
	        return this._unpublished_events['ev_'+event_id];
	    }

	    this._setApiResponse = function (response)
	    {
	        this._load(response.data);
	        this._page        = response.page;
	        this._page_size   = response.page_size;
	        this._total_pages = response.total_pages;
	    }

	    this.getPagesInfo = function()
	    {
	        return { page: this._page, total_pages: this._total_pages };
	    }

	    this.delete = function(event_id)
	    {
	        var item = this.get(event_id);
	        delete this._unpublished_events['ev_'+event_id];
	        self.trigger(self.LOAD_STORE);
	        return item;
	    }

	    this._load = function(events) {
	        this.clear();
	        // update model
	        for(var e of events) {
	            // we need to make the array key a string to keep the order of the items
	            this._unpublished_events['ev_'+e.id] = e;
	        }
	    }

	    var self = this;

	    api.on(api.RETRIEVED_UNPUBLISHED_EVENTS,function(response) {
	        console.log(api.RETRIEVED_UNPUBLISHED_EVENTS);
	        self._setApiResponse(response);
	        self.trigger(self.LOAD_STORE);
	    });
	}


	var store = new unpublishedEventsStore();

	module.exports = store;

	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1)))

/***/ },
/* 42 */
/***/ function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag

	// load the styles
	var content = __webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module \"!!./../../node_modules/css-loader/index.js!./../../node_modules/sass-loader/index.js!./summit-admin-schedule.scss\""); e.code = 'MODULE_NOT_FOUND'; throw e; }()));
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(44)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!./../../node_modules/css-loader/index.js!./../../node_modules/sass-loader/index.js!./summit-admin-schedule.scss", function() {
				var newContent = require("!!./../../node_modules/css-loader/index.js!./../../node_modules/sass-loader/index.js!./summit-admin-schedule.scss");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ },
/* 43 */,
/* 44 */
/***/ function(module, exports, __webpack_require__) {

	/*
		MIT License http://www.opensource.org/licenses/mit-license.php
		Author Tobias Koppers @sokra
	*/
	var stylesInDom = {},
		memoize = function(fn) {
			var memo;
			return function () {
				if (typeof memo === "undefined") memo = fn.apply(this, arguments);
				return memo;
			};
		},
		isOldIE = memoize(function() {
			return /msie [6-9]\b/.test(window.navigator.userAgent.toLowerCase());
		}),
		getHeadElement = memoize(function () {
			return document.head || document.getElementsByTagName("head")[0];
		}),
		singletonElement = null,
		singletonCounter = 0,
		styleElementsInsertedAtTop = [];

	module.exports = function(list, options) {
		if(false) {
			if(typeof document !== "object") throw new Error("The style-loader cannot be used in a non-browser environment");
		}

		options = options || {};
		// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
		// tags it will allow on a page
		if (typeof options.singleton === "undefined") options.singleton = isOldIE();

		// By default, add <style> tags to the bottom of <head>.
		if (typeof options.insertAt === "undefined") options.insertAt = "bottom";

		var styles = listToStyles(list);
		addStylesToDom(styles, options);

		return function update(newList) {
			var mayRemove = [];
			for(var i = 0; i < styles.length; i++) {
				var item = styles[i];
				var domStyle = stylesInDom[item.id];
				domStyle.refs--;
				mayRemove.push(domStyle);
			}
			if(newList) {
				var newStyles = listToStyles(newList);
				addStylesToDom(newStyles, options);
			}
			for(var i = 0; i < mayRemove.length; i++) {
				var domStyle = mayRemove[i];
				if(domStyle.refs === 0) {
					for(var j = 0; j < domStyle.parts.length; j++)
						domStyle.parts[j]();
					delete stylesInDom[domStyle.id];
				}
			}
		};
	}

	function addStylesToDom(styles, options) {
		for(var i = 0; i < styles.length; i++) {
			var item = styles[i];
			var domStyle = stylesInDom[item.id];
			if(domStyle) {
				domStyle.refs++;
				for(var j = 0; j < domStyle.parts.length; j++) {
					domStyle.parts[j](item.parts[j]);
				}
				for(; j < item.parts.length; j++) {
					domStyle.parts.push(addStyle(item.parts[j], options));
				}
			} else {
				var parts = [];
				for(var j = 0; j < item.parts.length; j++) {
					parts.push(addStyle(item.parts[j], options));
				}
				stylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};
			}
		}
	}

	function listToStyles(list) {
		var styles = [];
		var newStyles = {};
		for(var i = 0; i < list.length; i++) {
			var item = list[i];
			var id = item[0];
			var css = item[1];
			var media = item[2];
			var sourceMap = item[3];
			var part = {css: css, media: media, sourceMap: sourceMap};
			if(!newStyles[id])
				styles.push(newStyles[id] = {id: id, parts: [part]});
			else
				newStyles[id].parts.push(part);
		}
		return styles;
	}

	function insertStyleElement(options, styleElement) {
		var head = getHeadElement();
		var lastStyleElementInsertedAtTop = styleElementsInsertedAtTop[styleElementsInsertedAtTop.length - 1];
		if (options.insertAt === "top") {
			if(!lastStyleElementInsertedAtTop) {
				head.insertBefore(styleElement, head.firstChild);
			} else if(lastStyleElementInsertedAtTop.nextSibling) {
				head.insertBefore(styleElement, lastStyleElementInsertedAtTop.nextSibling);
			} else {
				head.appendChild(styleElement);
			}
			styleElementsInsertedAtTop.push(styleElement);
		} else if (options.insertAt === "bottom") {
			head.appendChild(styleElement);
		} else {
			throw new Error("Invalid value for parameter 'insertAt'. Must be 'top' or 'bottom'.");
		}
	}

	function removeStyleElement(styleElement) {
		styleElement.parentNode.removeChild(styleElement);
		var idx = styleElementsInsertedAtTop.indexOf(styleElement);
		if(idx >= 0) {
			styleElementsInsertedAtTop.splice(idx, 1);
		}
	}

	function createStyleElement(options) {
		var styleElement = document.createElement("style");
		styleElement.type = "text/css";
		insertStyleElement(options, styleElement);
		return styleElement;
	}

	function createLinkElement(options) {
		var linkElement = document.createElement("link");
		linkElement.rel = "stylesheet";
		insertStyleElement(options, linkElement);
		return linkElement;
	}

	function addStyle(obj, options) {
		var styleElement, update, remove;

		if (options.singleton) {
			var styleIndex = singletonCounter++;
			styleElement = singletonElement || (singletonElement = createStyleElement(options));
			update = applyToSingletonTag.bind(null, styleElement, styleIndex, false);
			remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true);
		} else if(obj.sourceMap &&
			typeof URL === "function" &&
			typeof URL.createObjectURL === "function" &&
			typeof URL.revokeObjectURL === "function" &&
			typeof Blob === "function" &&
			typeof btoa === "function") {
			styleElement = createLinkElement(options);
			update = updateLink.bind(null, styleElement);
			remove = function() {
				removeStyleElement(styleElement);
				if(styleElement.href)
					URL.revokeObjectURL(styleElement.href);
			};
		} else {
			styleElement = createStyleElement(options);
			update = applyToTag.bind(null, styleElement);
			remove = function() {
				removeStyleElement(styleElement);
			};
		}

		update(obj);

		return function updateStyle(newObj) {
			if(newObj) {
				if(newObj.css === obj.css && newObj.media === obj.media && newObj.sourceMap === obj.sourceMap)
					return;
				update(obj = newObj);
			} else {
				remove();
			}
		};
	}

	var replaceText = (function () {
		var textStore = [];

		return function (index, replacement) {
			textStore[index] = replacement;
			return textStore.filter(Boolean).join('\n');
		};
	})();

	function applyToSingletonTag(styleElement, index, remove, obj) {
		var css = remove ? "" : obj.css;

		if (styleElement.styleSheet) {
			styleElement.styleSheet.cssText = replaceText(index, css);
		} else {
			var cssNode = document.createTextNode(css);
			var childNodes = styleElement.childNodes;
			if (childNodes[index]) styleElement.removeChild(childNodes[index]);
			if (childNodes.length) {
				styleElement.insertBefore(cssNode, childNodes[index]);
			} else {
				styleElement.appendChild(cssNode);
			}
		}
	}

	function applyToTag(styleElement, obj) {
		var css = obj.css;
		var media = obj.media;
		var sourceMap = obj.sourceMap;

		if(media) {
			styleElement.setAttribute("media", media)
		}

		if(styleElement.styleSheet) {
			styleElement.styleSheet.cssText = css;
		} else {
			while(styleElement.firstChild) {
				styleElement.removeChild(styleElement.firstChild);
			}
			styleElement.appendChild(document.createTextNode(css));
		}
	}

	function updateLink(linkElement, obj) {
		var css = obj.css;
		var media = obj.media;
		var sourceMap = obj.sourceMap;

		if(sourceMap) {
			// http://stackoverflow.com/a/26603875
			css += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + " */";
		}

		var blob = new Blob([css], { type: "text/css" });

		var oldSrc = linkElement.href;

		linkElement.href = URL.createObjectURL(blob);

		if(oldSrc)
			URL.revokeObjectURL(oldSrc);
	}


/***/ },
/* 45 */
/***/ function(module, exports, __webpack_require__) {

	var riot = __webpack_require__(1);

	riot.tag2('schedule-admin-view-schedule-event', '<div class="event resizable event-published unselectable" id="event_{data.id}" data-id="{data.id}" riot-style="position:absolute; top: {getEventTop()}; left: {getEventLeft()}; height: {getEventHeight()}"> <div class="ui-resizable-handle ui-resizable-n" title="{data.start_datetime.format(\'hh:mm a\')}"> <span class="ui-icon ui-icon-triangle-1-n"></span> </div> <div class="event-buttons"> <a class="unpublish-event-btn" title="unpublish event" data-event-id="{data.id}"><i class="fa fa-times"></i></a> <a href="summit-admin/{parent.summit.id}/events/{data.id}" class="edit-event-btn" title="edit event"> <i class="fa fa-pencil-square-o"></i> </a> </div> <div class="event-inner-body"> <a id="popover_{data.id}" data-content="{getPopoverContent()}" title="{data.title}" data-toggle="popover">{data.title}</a> </div> <div class="ui-resizable-handle ui-resizable-s" title="{data.end_datetime.format(\'hh:mm a\')}"> <span class="ui-icon ui-icon-triangle-1-s"></span> </div> </div>', '', '', function(opts) {

	    this.data          = opts.data;
	    this.dispatcher    = parent.dispatcher;
	    this.summit        = parent.summit;
	    this.minute_pixels = parseInt(opts.minute_pixels);
	    this.interval      = parseInt(opts.interval);
	    var self           = this;

	    this.on('mount', function() {

	    });

	    this.getEventTop = function() {
	        var start_time    = self.data.start_datetime;
	        var start_hour    = start_time.hour();
	        var start_minutes = start_time.minute();
	        var r             = start_minutes % self.interval;
	        var start_minutes = start_minutes - r;
	        var target        = $('#time_slot_container_'+self.pad(start_hour,2)+'_'+self.pad(start_minutes,2));
	        var top           = parseInt(target.position().top) + ( r * self.minute_pixels);
	        return top+'px';
	    }.bind(this)

	    this.getEventLeft = function() {
	        var start_time    = self.data.start_datetime;
	        var start_hour    = start_time.hour();
	        var start_minutes = start_time.minute();
	        var r             = start_minutes % self.interval;
	        var start_minutes = start_minutes - r;
	        var target        = $('#time_slot_container_'+self.pad(start_hour,2)+'_'+self.pad(start_minutes,2));
	        var left          = target.position().left;
	        return left+'px';
	    }.bind(this)

	    this.getEventHeight = function() {
	        var start_time = self.data.start_datetime;
	        var end_time   = self.data.end_datetime;
	        var duration   = moment.duration(end_time.diff(start_time));
	        var minutes    = duration.asMinutes();
	        console.log('event duration '+ minutes);
	        return (parseInt(minutes) * self.minute_pixels)+'px';
	    }.bind(this)

	    this.getPopoverContent = function() {
	        var res = '<div class="row"><div class="col-md-12">'+self.data.description+'</div></div>';
	        if(typeof(self.data.speakers) !== 'undefined') {
	            res += '<div class="row"><div class="col-md-12"><b>Speakers</b></div></div>';
	            for(var idx in self.data.speakers) {
	                var speaker = self.data.speakers[idx];
	                res += '<div class="row"><div class="col-md-12">'+ speaker.name+'</div></div>';
	            }
	        }
	        return res;
	    }.bind(this)

	    this.pad = function(num, size) {
	        var s = num+"";
	        while (s.length < size) s = "0" + s;
	        return s;
	    }.bind(this)

	}, '{ }');

/***/ },
/* 46 */
/***/ function(module, exports, __webpack_require__) {

	var riot = __webpack_require__(1);

	riot.tag2('schedule-admin-view-published-filters', '<div class="row"> <div class="col-md-12" style="margin:10px 0;"> <div class="input-group" style="width: 100%;"> <input data-rule-required="true" data-rule-minlength="3" type="text" id="published_search_term" class="form-control input-global-search" placeholder="Search for published Events"> <span class="input-group-btn" style="width: 5%;"> <button class="btn btn-default btn-global-search" id="search_published"><i class="fa fa-search"></i></button> <button class="btn btn-default btn-global-search-clear" onclick="{clearClicked}"> <i class="fa fa-times"></i> </button> <button class="btn btn-default" data-toggle="modal" data-target="#empty_spots_modal">Find Empty</button> </span> </div> </div> </div> <div class="row" style="margin-bottom: 35px;"> <div class="col-md-6"> <label for="select_day">Day</label> <select id="select_day" name="select_day" style="width: 80%"> <option value="">-- Select A Day --</option> <option value="{day.date}" each="{key, day in summit.dates}">{day.label}</option> </select> </div> <div class="col-md-6"> <label for="select_venue">Venue</label> <select id="select_venue" name="select_venue" style="width: 80%"> <option value="">-- Select A Venue --</option> <option value="{id}" each="{id, location in summit.locations}">{location.name}</option> <option value="0">TBA</option> </select> </div> </div> <div id="empty_spots_modal" class="modal fade" role="dialog"> <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title">Find Empty Spots</h4> </div> <div class="modal-body"> <div class="row" style="margin-bottom: 35px;"> <div class="col-md-6"> <label for="select_day_modal">Day</label> <select id="select_day_modal" style="width: 80%"> <option value="">-- Any Day --</option> <option value="{day.date}" each="{key, day in summit.dates}">{day.label}</option> </select> </div> <div class="col-md-6"> <label for="select_venue_modal">Venue</label> <select id="select_venue_modal" style="width: 80%"> <option value="">-- Any Venue --</option> <option value="{id}" each="{id, location in summit.locations}">{location.name}</option> </select> </div> </div> <div class="row" style="margin-bottom: 35px;"> <div class="col-md-3"> <label for="start_time_modal">From </label> <input type="text" id="start_time_modal" value="06:00" style="width: 60%"> </div> <div class="col-md-3"> <label for="end_time_modal">To </label> <input type="text" id="end_time_modal" value="23:30" style="width: 60%"> </div> <div class="col-md-6"> <label for="end_time_modal">Gap (minutes)</label> <input type="{\'number\'}" id="length_modal" min="15" max="240" step="15" value="60" style="width: 60px"> </div> </div> </div> <div class="modal-footer"> <button type="button" id="search_empty" class="btn btn-default" data-dismiss="modal">Find</button> </div> </div> </div> </div>', '', '', function(opts) {
	                this.summit     = opts.summit;
	                this.day        = '';
	                this.location_id = '';
	                this.dispatcher = opts.dispatcher;
	                var self        = this;

	                this.on('updated', function(){
	                    $('#select_day').val(self.day);
	                    $('#select_venue').val(self.location_id);
	                });

	                this.on('mount', function(){
	                    $(function() {
	                        $('#select_day').change(function(e){
	                            self.day         = $('#select_day').val();
	                            self.location_id = $('#select_venue').val();
	                            self.clearClicked();
	                            self.doFilter();
	                        });

	                        $('#select_venue').change(function(e) {
	                            self.day         = $('#select_day').val();
	                            self.location_id = $('#select_venue').val();
	                            self.clearClicked();
	                            self.doFilter();
	                        });

	                        $('#search_published').click(function(e) {
	                            var search_term = $('#published_search_term').val();
	                            if (search_term) {
	                                self.doSearch(search_term);
	                            }
	                        });

	                        $("#published_search_term").keydown(function (e) {
	                            if (e.keyCode == 13) {
	                                $('#search_published').click();
	                            }
	                        });

	                        $('#search_empty').click(function(e) {
	                            self.doSearchEmpty();
	                        });

	                        $('#start_time_modal,#end_time_modal').datetimepicker({
	                            datepicker:false,
	                            format:'H:i',
	                            minTime:'06:00',
	                            maxTime:'23:30',
	                            step: 15
	                        });

	                        self.lockHash();

	                        self.doFilter();
	                    });
	                });

	                this.doFilter = function() {
	                    if(self.day === '' || self.location_id === '') return;
	                    $('body').ajax_loader();

	                    self.dispatcher.publishedEventsFilterChanged(self.summit.id, self.day ,self.location_id);
	                }.bind(this)

	                this.doSearch = function(term) {
	                    $('body').ajax_loader();
	                    $('#schedule_container').hide();
	                    $('#search_results').show();
	                    self.dispatcher.publishedEventsSearch(self.summit.id, term);
	                }.bind(this)

	                this.clearClicked = function(){
	                    $('#published_search_term').val('');
	                    window.location.hash = '';
	                    $('#schedule_container').show();
	                    $('#search_results').hide();
	                }.bind(this)

	                this.doSearchEmpty = function() {
	                    $('body').ajax_loader();
	                    $('#schedule_container').hide();
	                    $('#empty_spots').show();
	                    window.location.hash = '';

	                    var days = [];
	                    var venues = [];
	                    var start_time = $('#start_time_modal').val();
	                    var end_time = $('#end_time_modal').val();
	                    var length = $('#length_modal').val() * 60;

	                    if ($('#select_day_modal').val() == '') {
	                        $("#select_day_modal option").each(function(){
	                            if ($(this).val() != '') days.push($(this).val());
	                        });
	                    } else {
	                        days.push($('#select_day_modal').val());
	                    }

	                    if ($('#select_venue_modal').val() == '') {
	                        $("#select_venue_modal option").each(function(){
	                            if ($(this).val() != '') venues.push($(this).val());
	                        });
	                    } else {
	                        venues.push($('#select_venue_modal').val());
	                    }

	                    self.dispatcher.publishedEventsSearchEmpty(self.summit.id,days,start_time,end_time,venues,length);
	                }.bind(this)

	                this.lockHash = function() {

	                    var hash = $(window).url_fragment('getParams');

	                    if(!$.isEmptyObject(hash)){
	                        for(var key in hash) {
	                            var value = hash[key];

	                            switch(key) {
	                                case 'day':
	                                    $('#select_day').val(value);
	                                    self.day = value;
	                                    break;
	                                case 'venue':
	                                    $('#select_venue').val(value);
	                                    self.location_id = value;
	                                    break;
	                            }
	                        }
	                    }
	                }.bind(this)

	                self.dispatcher.on(self.dispatcher.PUBLISHED_EVENTS_DEEP_LINK, function()
	                {
	                    $('#published_search_term').val('');
	                    $('#schedule_container').show();
	                    $('#search_results').hide();
	                    $('#empty_spots').hide();

	                    $('body').ajax_loader();

	                    self.lockHash();
	                    self.doFilter();

	                });

	}, '{ }');

/***/ },
/* 47 */
/***/ function(module, exports, __webpack_require__) {

	var riot = __webpack_require__(1);

	riot.tag2('schedule-admin-view-published', '<div class="row" id="schedule_container"> <div class="col-md-12"> <table id="day_schedule" class="unselectable"> <tbody> <tr> <td class="times-col"> <div each="{time_slots}" class="time-slot" id="time_slot_{format(⁗HH_mm⁗)}">{format(⁗hh:mm A⁗)}</div> </td> <td class="events-col col-md-12"> <div each="{time_slots}" class="time-slot-container" data-time="{format(\'HH:mm\')}" id="time_slot_container_{format(\'HH_mm\')}"></div> <schedule-admin-view-schedule-event each="{key, e in published_store.all()}" data="{e}" minute_pixels="{parent.minute_pixels}" interval="{parent.interval}"></schedule-admin-view-schedule-event> </td> </tr> </tbody> </table> </div> </div>', '', '', function(opts) {

	        this.day               = null;
	        this.location          = null;
	        this.start_time        = moment(opts.start_time, 'HH:mm');
	        this.end_time          = moment(opts.end_time, 'HH:mm');
	        this.interval          = parseInt(opts.interval);
	        this.minute_pixels     = parseInt(opts.minute_pixels);
	        this.slot_width        = parseInt(opts.slot_width);
	        this.dispatcher        = opts.dispatcher;
	        this.published_store   = opts.published_store;
	        this.unpublished_store = opts.unpublished_store;
	        this.summit            = summit;
	        this.time_slots        = [];
	        this.api               = opts.api;
	        var self               = this;
	        var done               = false;

	        var slot = this.start_time ;
	        do
	        {
	            this.time_slots.push(slot);
	            slot = slot.clone();
	            slot.add('m', this.interval);
	            done = slot.isAfter(this.end_time);
	        }while(!done);

	        this.on('mount', function(){
	            $(function() {

	                self.createDraggable($(".event"));
	                self.slot_width = $('.time-slot-container').width();
	                $( ".time-slot-container" ).droppable({
	                    hoverClass: "ui-state-hover",
	                    accept: function(){
	                        return self.published_store.currentDay() !== null && self.published_store.currentLocation();
	                    },
	                    drop: function( e, ui ) {

	                        var element = $(ui.draggable);
	                        var id      = parseInt(element.attr('data-id'));
	                        var target  = $(this);
	                        var is_published_event = true;

	                        var top    = target.position().top;
	                        var left   = target.position().left;

	                        element.addClass('event-published');
	                        var start_hour = target.attr('data-time');
	                        var day        = self.published_store.currentDay();
	                        var minutes    = (parseInt(element.css('height'))/ self.minute_pixels);
	                        var end_time   = moment(day+' '+start_hour, 'YYYY-MM-DD HH:mm').add('m', minutes);
	                        var start_time = moment(day+' '+start_hour, 'YYYY-MM-DD HH:mm');

	                        console.log('star time '+ start_time.format('HH:mm'));
	                        console.log('end time ' + end_time.format('HH:mm'));

	                        for(var id2 in self.published_store.all())
	                        {
	                            if(parseInt(id2) === parseInt(id)) continue;
	                            var e           = self.published_store.get(id2);
	                            var start_time2 = e.start_datetime;
	                            var end_time2   = e.end_datetime;
	                            if(start_time.isBefore(end_time2) && end_time.isAfter(start_time2)) {
	                                console.log('overlapped!!!');
	                                return false;
	                            }
	                        }

	                        element.appendTo($('.events-col'));
	                        element.css('position','absolute');
	                        element.css('top', top + 1);
	                        element.css('left', left);

	                        if(element.hasClass('event-unpublished')) {
	                            is_published_event = false;
	                            element.removeClass('event-unpublished');
	                            element.addClass('event-published');
	                            $('.ui-resizable-n', element).show();
	                            $('.ui-resizable-s', element).show();
	                            $('.unpublish-event-btn-container', element).show();
	                            self.published_store.add(self.unpublished_store.delete(id));
	                        }

	                        var event            = self.published_store.get(id);
	                        event.start_datetime = start_time;
	                        event.end_datetime   = end_time;

	                        if( typeof element.resizable( "instance" ) == 'undefined'){
	                            self.createResizable(element);
	                        }

	                        $('.ui-resizable-n', element).attr('title', event.start_datetime.format('hh:mm a'));
	                        $('.ui-resizable-s', element).attr('title', event.end_datetime.format('hh:mm a'));

	                        event.location_id = self.published_store.currentLocation();
	                        self.api.publish(self.summit.id, event, is_published_event);
	                    }
	                });

	                $( "body" ).on( "click", ".unpublish-event-btn",function() {
	                    var id = $(this).attr('data-event-id');
	                    swal({
	                        title: "Are you sure?",
	                        text: "to unpublish this event from summit schedule!",
	                        type: "warning",
	                        showCancelButton: true,
	                        confirmButtonColor: "#DD6B55",
	                        confirmButtonText: "Yes, unpublish it!",
	                        closeOnConfirm: false
	                        }, function(){
	                            self.dispatcher.unPublishEvent(self.summit.id, id);
	                            $('#event_'+id).remove();
	                            swal("Deleted!", "Your event was unpublished.", "success");
	                        });
	                });
	            });

	            $( window ).resize(function() {
	                self.slot_width = $('.time-slot-container').width();
	                $(".event-published").css('width', self.slot_width);
	                $(".ui-resizable").resizable( "option", "maxWidth", self.slot_width );
	                $(".ui-resizable").resizable( "option", "minWidth", self.slot_width );
	            });

	        });

	        this.createResizable = function(selector) {

	            selector.each(function(){
	                var element = $(this);
	                var id      = element.attr('data-id');
	                var top = $(".ui-resizable-n", element).uitooltip({
	                    tooltipClass: "tooltip-n-"+id,
	                    track: true,
	                    position: { my: "left+15 center", at: "right center"  }
	                });
	                var bottom = $(".ui-resizable-s", element).uitooltip({
	                    tooltipClass: "tooltip-s-"+id,
	                    track: true,
	                    position: { my: "left+15 center", at: "right center"  }
	                });
	                top.addClass('top');
	                bottom.addClass('bottom');
	            });

	            selector.resizable({
	                containment: ".events-col",
	                maxWidth: self.slot_width,
	                minWidth: self.slot_width,
	                minHeight: self.minute_pixels * self.interval,
	                maxHeight: null,
	                grid: self.minute_pixels,
	                handles: {
	                    n: ".ui-resizable-n",
	                    s: ".ui-resizable-s"
	                },
	                resize: function(e, ui) {

	                    var element      = $(ui.element);
	                    var id           = element.attr('data-id');
	                    var size         = ui.size;
	                    var pos          = element.offset();
	                    var bottom       = pos.top + size.height;
	                    var minutes      = ( parseInt(size.height) / self.minute_pixels);
	                    var overlapped   = false;
	                    var container    = null;
	                    var original_h   = ui.originalSize.height;
	                    ui.size.width    = ui.originalSize.width = self.slot_width;
	                    ui.position.left = ui.originalPosition.left;
	                    console.log('position top ' + pos.top + ' height ' + size.height + ' bottom ' + bottom+' original_h '+original_h);

	                    $('.time-slot-container').each(function(){
	                        var top       = $(this).offset().top;
	                        var bottom    = parseInt(top) + (self.minute_pixels * self.interval);
	                        if(top <= pos.top &&  pos.top < bottom)
	                        {
	                            container = $(this);
	                            console.log(' container top '+ top + ' container bottom '+bottom+ ' time '+container.attr('data-time'));
	                            return false;
	                        }
	                    });
	                    var day        = self.published_store.currentDay();
	                    var start_hour = container.attr('data-time');
	                    var start_time = moment(day+' '+start_hour, 'YYYY-MM-DD HH:mm');

	                    if(pos.top > container.offset().top) {
	                       var delta_minutes = ( pos.top - container.offset().top) / self.minute_pixels;
	                       console.log(' adding '+ delta_minutes+' minutes to start time '+ container.attr('data-time'));
	                       start_time = start_time.add('m', delta_minutes)
	                    }

	                    var end_time   = start_time.clone().add('m', minutes);

	                    var overlapped_id = null;
	                    for(var id2 in self.published_store.all())
	                    {
	                        if(parseInt(id2) === parseInt(id)) continue;
	                        var event2      = self.published_store.get(id2);
	                        var start_time2 = event2.start_datetime;
	                        var end_time2   = event2.end_datetime;
	                        if(start_time.isSameOrBefore(end_time2) && end_time.isSameOrAfter(start_time2)) {

	                            overlapped    = true;
	                            overlapped_id = parseInt(id2);
	                            console.log('overlapped id '+overlapped_id+' !!!');
	                            break;
	                        }
	                    }

	                    if(overlapped)
	                    {
	                        var event2         = self.published_store.get(overlapped_id);
	                        var start_time2    = event2.start_datetime;
	                        var end_time2      = event2.end_datetime;
	                        var delta1         = moment.duration(end_time2.diff(start_time)).asMinutes();
	                        var delta2         = moment.duration(end_time.diff(start_time2)).asMinutes();
	                        var delta          = delta1 < delta2 ? delta1 : delta2;
	                        console.log(' delta minutes '+delta);
	                        element.resizable( "option", "maxHeight", size.height - ( ( delta ) * self.minute_pixels ));
	                        return false;
	                    }

	                    var event            = self.published_store.get(id);
	                    event.start_datetime = start_time;
	                    event.end_datetime   = end_time;
	                    $('.ui-resizable-n', element).attr('title', event.start_datetime.format('hh:mm a'));
	                    $('.ui-resizable-s', element).attr('title', event.end_datetime.format('hh:mm a'));
	                    $('.ui-tooltip-content','.tooltip-n-'+id).html(event.start_datetime.format('hh:mm a'));
	                    $('.ui-tooltip-content','.tooltip-s-'+id).html(event.end_datetime.format('hh:mm a'));
	                },
	                start: function( e, ui )
	                {
	                    var element = $(ui.element);
	                    element.resizable( "option", "maxHeight", null );
	                    var original_h = ui.originalSize.height;
	                    console.log('start original_h '+original_h);

	                    $(this).attr('prev-height',$(this).css('height'));
	                    $(this).attr('prev-pos-top',$(this).position().top);
	                },
	                stop: function( e, ui ) {
	                    var element = $(ui.element);
	                    var id      = element.attr('data-id');
	                    var event   = self.published_store.get(id);
	                    $('.ui-resizable-n', element).attr('title', event.start_datetime.format('hh:mm a'));
	                    $('.ui-resizable-s', element).attr('title', event.end_datetime.format('hh:mm a'));
	                    event.location_id = self.published_store.currentLocation();
	                    self.api.publish(self.summit.id, event, true);
	                    console.log('stop');
	                }
	            });
	        }.bind(this)

	        this.createDraggable = function(selector) {
	            selector.draggable({
	                containment: "document",
	                cursor: "move",
	                helper: "clone",
	                opacity: 0.5,
	                start: function(){
	                    $(this).attr('prev-height',$(this).css('height'));
	                    $(this).attr('prev-pos-top',$(this).position().top);
	                }
	            });
	        }.bind(this)

	        self.published_store.on(self.published_store.LOAD_STORE,function() {
	            console.log('UI: '+self.published_store.LOAD_STORE);

	            $(".event-published").resizable("destroy");
	            $(".event-published").draggable("destroy");
	            $(".event-published").remove();

	            self.update();
	            $(".event-published").css('width', $('.time-slot-container').width());
	            self.createDraggable($(".event-published"));

	            self.createResizable($(".event-published"));

	            $('[data-toggle="popover"]').popover({
	                trigger: 'hover focus',
	                html: true,
	                container: 'body',
	                placement: 'auto',
	                animation: true,
	                template : '<div class="popover" role="tooltip"><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
	            });

	            $('.event').hover(function(){
	                $('.event-buttons',this).stop().animate({width: '20px'}, 400)
	            }, function(){
	                $('.event-buttons',this).stop().animate({width: '-0'}, 400)
	            });

	            $('body').ajax_loader('stop');

	            var hash = $(window).url_fragment('getParams');

	            if(hash){
	                for(var key in hash) {
	                    var value = hash[key];
	                    if (key == 'event') {
	                        $('body').animate({
	                            scrollTop: ($("#event_"+value).offset().top - 100)
	                        }, 2000, function() {
	                            $("#event_"+value).effect("highlight", {}, 2000);
	                        });
	                    } else if (key == 'time') {
	                        $('body').animate({
	                        scrollTop: ($('[data-time="'+value+'"]').offset().top - 100)
	                        }, 2000);
	                    }
	                }
	            }

	        });

	}, '{ }');

/***/ },
/* 48 */
/***/ function(module, exports, __webpack_require__) {

	var riot = __webpack_require__(1);

	riot.tag2('schedule-admin-view-published-results', '<div id="search_results" style="display:none"> <h4>Search Results</h4> <div class="list-group"> <a href="#" onclick="{parent.deepLinkToEvent}" class="list-group-item" each="{key, e in published_store.results()}"> <h4 class="list-group-item-heading" id="popover_{e.id}" data-content="{getPopoverContent(e)}" title="{e.title}" data-toggle="popover">{e.title}</h4> </a> </div> </div>', '', '', function(opts) {

	        this.published_store   = opts.published_store;
	        this.summit            = summit;
	        this.api               = opts.api;
	        this.dispatcher        = opts.dispatcher;
	        var self               = this;

	        self.published_store.on(self.published_store.LOAD_RESULTS,function() {
	            console.log('UI: '+self.published_store.LOAD_RESULTS);
	            self.update();
	            $('[data-toggle="popover"]').popover({
	                trigger: 'hover focus',
	                html: true,
	                container: 'body',
	                placement: 'auto',
	                animation: true,
	                template : '<div class="popover" role="tooltip"><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
	            });

	            $('body').ajax_loader('stop');

	        });

	        this.getPopoverContent = function(event) {
	            var res = '<div class="row"><div class="col-md-12">'+event.description+'</div></div>';
	            if(typeof(event.speakers) !== 'undefined') {
	                res += '<div class="row"><div class="col-md-12"><b>Speakers</b></div></div>';
	                for(var idx in event.speakers) {
	                    var speaker = event.speakers[idx];
	                    res += '<div class="row"><div class="col-md-12">'+ speaker.name+'</div></div>';
	                }
	            }
	            return res;
	        }.bind(this)

	        this.deepLinkToEvent = function(event) {
	            var item = event.item;

	            $(window).url_fragment('setParam','day', item.e.start_date);
	            $(window).url_fragment('setParam','venue', item.e.location_id);
	            $(window).url_fragment('setParam','event', item.e.id);
	            window.location.hash =  $(window).url_fragment('serialize');

	            $('[data-toggle="popover"]').each(function() {
	                $(this).popover('hide');
	            });

	            self.dispatcher.publishedEventsDeepLink();
	        }.bind(this)

	}, '{ }');

/***/ },
/* 49 */
/***/ function(module, exports, __webpack_require__) {

	var riot = __webpack_require__(1);

	riot.tag2('schedule-admin-view-empty-spots', '<div id="empty_spots" style="display:none"> <h4>Empty Spots</h4> <div each="{location, spot in published_store.empty_spots()}"> <hr> <h5>{summit.locations[location].name}</h5> <div class="list-group"> <a href="#" onclick="{parent.deepLinkToSpot}" class="list-group-item" each="{spot}"> <h4 class="list-group-item-heading"> {summit.dates[day].label} - From: {moment(time, \'HH:mm\').format(⁗hh:mm A⁗)} - Gap: {gap} </h4> </a> </div> </div> </div>', '', '', function(opts) {

	        this.published_store   = opts.published_store;
	        this.summit            = summit;
	        this.api               = opts.api;
	        this.dispatcher        = opts.dispatcher;
	        var self               = this;

	        self.published_store.on(self.published_store.LOAD_EMPTY_SPOTS,function() {
	            console.log('UI: '+self.published_store.LOAD_EMPTY_SPOTS);
	            self.update();
	            $('body').ajax_loader('stop');
	        });

	        this.deepLinkToSpot = function(event) {
	            var item = event.item;

	            $(window).url_fragment('setParam','day', item.day);
	            $(window).url_fragment('setParam','venue', item.location_id);
	            $(window).url_fragment('setParam','time', item.time);
	            window.location.hash =  $(window).url_fragment('serialize');

	            self.dispatcher.publishedEventsDeepLink();
	        }.bind(this)

	}, '{ }');

/***/ },
/* 50 */
/***/ function(module, exports, __webpack_require__) {

	var riot = __webpack_require__(1);

	riot.tag2('schedule-admin-view-unpublished-filters', '<div class="row filters"> <div class="col-md-12" style="margin:10px 0;"> <div class="input-group" style="width: 100%;"> <input data-rule-required="true" data-rule-minlength="3" type="text" id="unpublished_search_term" class="form-control input-global-search" placeholder="Search for unpublished Events"> <span class="input-group-btn" style="width: 5%;"> <button class="btn btn-default btn-global-search" id="search_unpublished"><i class="fa fa-search"></i></button> <button class="btn btn-default btn-global-search-clear" onclick="{clearClicked}"> <i class="fa fa-times"></i> </button> </span> </div> </div> </div> <div class="row filters"> <div class="col-md-5"> <label for="select_unpublished_events_source">Source</label> <select id="select_unpublished_events_source" name="select_unpublished_events_source" style="width: 70%" value="presentations"> <option value="">-- Select An Event Source --</option> <option value="tracks">Tracks List</option> <option value="presentations" selected>Presentations</option> <option value="events">Summit Events</option> </select> </div> <div class="col-md-7"> <div id="track_list_col" style="display:none;"> <label for="select_track_list">Track Lists</label> <select id="select_track_list" name="select_track_list" style="width: 70%"> <option value="">-- All --</option> <option each="{id, list in summit.track_lists}" value="{id}">{list.name}</option> </select> </div> <div id="event_type_col" style="display:none;"> <label for="select_event_type">Event Types</label> <select id="select_event_type" name="select_event_type" style="width: 70%"> <option value="">-- All --</option> <option each="{id, type in summit.event_types}" if="{type.type != \'Presentation\'}" value="{id}">{type.type}</option> </select> </div> </div> </div> <div class="row filters"> <div class="col-md-5"> <label for="sort_list">Sort by</label> <select id="sort_list" name="sort_list" style="width: 70%" value="title"> <option value="SummitEvent.Title" selected>Title</option> <option value="SummitEvent.ID">Event Id</option> <option value="SummitEvent.StartDate">Start Date</option> </select> </div> <div class="col-md-7"> <button class="btn btn-primary btn-xs unpublished-events-refresh" title="refresh unpublished events"> &nbsp;Refresh&nbsp;<i class="fa fa-refresh"></i> </button> </div> </div>', '', '', function(opts) {
	        this.summit     = opts.summit;
	        this.api        = opts.api;
	        this.dispatcher = opts.dispatcher;
	        var self        = this;

	        this.on('mount', function(){
	            $(function() {
	                $('#select_unpublished_events_source').change(function(e){
	                    var source = $('#select_unpublished_events_source').val();
	                    var search_term = $('#unpublished_search_term').val();
	                    var order = $('#sort_list').val();

	                    self.dispatcher.unpublishedEventsSourceChanged(source);
	                    if(source === '') {
	                        $('#track_list_col').hide();
	                        return;
	                    }

	                    switch (source) {
	                        case 'tracks':
	                            $('#track_list_col').show();
	                            $('#event_type_col').hide();
	                            break;
	                        case 'events':
	                            $('#track_list_col').hide();
	                            $('#event_type_col').show();
	                            break;
	                        default:
	                            $('#track_list_col').hide();
	                            $('#event_type_col').hide();
	                            break;
	                    }

	                    self.doFilter(source,'',search_term,order);
	                });

	                $('#select_track_list').change(function(e){
	                    var source        = $('#select_unpublished_events_source').val();
	                    var track_list_id = $('#select_track_list').val();
	                    var search_term   = $('#unpublished_search_term').val();
	                    var order         = $('#sort_list').val();

	                    self.doFilter(source, track_list_id,search_term,order);
	                });

	                $('#select_event_type').change(function(e){
	                    var source        = $('#select_unpublished_events_source').val();
	                    var event_type_id = $('#select_event_type').val();
	                    var search_term   = $('#unpublished_search_term').val();
	                    var order         = $('#sort_list').val();

	                    self.doFilter(source, event_type_id,search_term,order);
	                });

	                $('.unpublished-events-refresh').click(function(e){
	                    var source       = $('#select_unpublished_events_source').val();
	                    var track_list_id = $('#select_track_list').val();
	                    var search_term = $('#unpublished_search_term').val();
	                    var order = $('#sort_list').val();

	                    if (source) {
	                        self.doFilter(source, track_list_id,search_term,order);
	                    }
	                });

	                $('#sort_list').change(function(e) {
	                    var source       = $('#select_unpublished_events_source').val();
	                    var track_list_id = $('#select_track_list').val();
	                    var search_term = $('#unpublished_search_term').val();
	                    var order = $('#sort_list').val();

	                    if (source) {
	                        self.doFilter(source, track_list_id,search_term,order);
	                    }
	                });

	                $('#search_unpublished').click(function(e) {
	                    var source       = $('#select_unpublished_events_source').val();
	                    var track_list_id = $('#select_track_list').val();
	                    var search_term = $('#unpublished_search_term').val();
	                    var order = $('#sort_list').val();

	                    if (source) {
	                        self.doFilter(source, track_list_id,search_term,order);
	                    } else {
	                        swal("Select a Source", "Please select a source to search on.", "warning");
	                    }
	                });

	                $("#unpublished_search_term").keydown(function (e) {
	                    if (e.keyCode == 13) {
	                        $('#search_unpublished').click();
	                    }
	                });

	                self.doFilter('presentations', '','','SummitEvent.Title');
	            });
	        });

	        this.doFilter = function(source, second_source, search_term, order)
	        {
	            $('body').ajax_loader();
	            self.api.getUnpublishedEventsBySource(self.summit.id, source ,second_source, search_term, order);
	        }.bind(this)

	        self.dispatcher.on(self.dispatcher.UNPUBLISHED_EVENTS_PAGE_CHANGED, function(page_nbr)
	        {
	            var source       = $('#select_unpublished_events_source').val();
	            var track_list_id = $('#select_track_list').val();
	            var search_term = $('#unpublished_search_term').val();
	            var order = $('#sort_list').val();

	            self.api.getUnpublishedEventsBySource(self.summit.id, source ,track_list_id, search_term, order, page_nbr, 10);
	        });

	        this.clearClicked = function(e){

	            $('#unpublished_search_term').val('');

	            var source       = $('#select_unpublished_events_source').val();
	            var track_list_id = $('#select_track_list').val();
	            var search_term = $('#unpublished_search_term').val();
	            var order = $('#sort_list').val();

	            if (source) {
	                self.doFilter(source, track_list_id,search_term,order);
	            }
	        }.bind(this)

	}, '{ }');

/***/ },
/* 51 */
/***/ function(module, exports, __webpack_require__) {

	var riot = __webpack_require__(1);

	riot.tag2('schedule-admin-view-unpublished-event', '<div class="event resizable event-unpublished unselectable" id="event_{data.id}" data-id="{data.id}"> <div class="ui-resizable-handle ui-resizable-n" style="display:none"> <span class="ui-icon ui-icon-triangle-1-n"></span> </div> <div class="event-buttons"> <a href="summit-admin/{parent.summit.id}/events/{data.id}" class="edit-event-btn" title="edit event"> <i class="fa fa-pencil-square-o"></i> </a> </div> <div class="event-inner-body"> <div class="event-title"> <a id="popover_{data.id}" data-content="{getPopoverContent()}" title="{data.title}" data-toggle="popover">{data.title}</a> </div> </div> <div class="ui-resizable-handle ui-resizable-s" style="display:none"> <span class="ui-icon ui-icon-triangle-1-s"></span> </div> </div>', '', '', function(opts) {

	        this.data          = opts.data;
	        this.summit        = parent.summit;
	        this.minute_pixels = parseInt(opts.minute_pixels);
	        this.interval      = parseInt(opts.interval);
	        var self           = this;

	        this.on('mount', function() {

	        });

	        this.getPopoverContent = function() {
	            var res = '<div class="row"><div class="col-md-12">'+self.data.description+'</div></div>';
	            if(typeof(self.data.speakers) !== 'undefined') {
	                res += '<div class="row"><div class="col-md-12"><b>Speakers</b></div></div>';
	                for(var idx in self.data.speakers) {
	                    var speaker = self.data.speakers[idx];
	                    res += '<div class="row"><div class="col-md-12">'+ speaker.name+'</div></div>';
	                }
	            }
	            return res;
	        }.bind(this)

	}, '{ }');

/***/ },
/* 52 */
/***/ function(module, exports, __webpack_require__) {

	var riot = __webpack_require__(1);

	riot.tag2('schedule-admin-view-unpublished', '<div if="{Object.keys(store.all()).length === 0}" class="no_matches">Sorry, found no matches for your search.</div> <div if="{Object.keys(store.all()).length > 0}"> <ul class="list-unstyled unpublished-events-list"> <li each="{key, e in store.all()}"> <schedule-admin-view-unpublished-event data="{e}" minute_pixels="{parent.minute_pixels}" interval="{parent.interval}"></schedule-admin-view-unpublished-event> </li> </ul> <div> <ul id="unpublished-events-pager"></ul> </div> </div>', '', '', function(opts) {

	        this.summit             = opts.summit;
	        this.minute_pixels      = opts.minute_pixels;
	        this.interval           = opts.interval;
	        this.api                = opts.api;
	        this.dispatcher         = opts.dispatcher;
	        this.store              = opts.unpublished_store;
	        var self                = this;

	        this.on('mount', function() {
	            $( window ).resize(function() {
	                 self.slot_width = $('.time-slot-container').width();
	                 $('.event-unpublished').css('width', self.slot_width);
	            });
	        });

	        self.store.on(self.store.LOAD_STORE,function() {
	            console.log('UI: '+self.store.LOAD_STORE);

	            $(".event-unpublished").remove();
	            var page_info = self.store.getPagesInfo();
	            self.update();
	            var options = {
	                bootstrapMajorVersion:3,
	                currentPage: page_info.page ,
	                totalPages: page_info.total_pages,
	                numberOfPages: 10,
	                onPageChanged: function(e,oldPage,newPage){
	                    $('#alert-content').text("Current page changed, old: "+oldPage+" new: "+newPage);
	                    console.log('page ' + newPage);
	                    self.dispatcher.unpublishedEventsPageChanged(newPage);
	                }
	            }

	            if (Object.keys(self.store.all()).length){
	                $('#unpublished-events-pager').bootstrapPaginator(options);
	            }

	            $('[data-toggle="popover"]').popover({
	                trigger: 'hover focus',
	                html: true,
	                container: 'body',
	                placement: 'auto',
	                animation: true,
	                template : '<div class="popover" role="tooltip"><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
	            });

	            $('.event').hover(function(){
	                $('.event-buttons',this).stop().animate({width: '20px'}, 400)
	            }, function(){
	                $('.event-buttons',this).stop().animate({width: '-0'}, 400)
	            });

	            self.createDraggable($(".event-unpublished"));
	            $('.event-unpublished').css('width', $('.time-slot-container').width());
	            $('body').ajax_loader('stop');
	        });

	        this.createDraggable = function(selector) {
	            selector.draggable({
	                containment: "document",
	                cursor: "move",
	                helper: "clone",
	                opacity: 0.5
	            });
	        }.bind(this)

	        self.dispatcher.on(self.dispatcher.UNPUBLISHED_EVENTS_SOURCE_CHANGED, function(source){
	            if(source === '')
	            {
	                $('.unpublished-events-list').hide();
	                $('#unpublished-events-pager').hide();
	            }
	            else
	            {
	                $('.unpublished-events-list').show();
	                $('#unpublished-events-pager').show();
	            }
	        });
	}, '{ }');

/***/ }
/******/ ]);